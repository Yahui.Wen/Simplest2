package test.models;

import main.game.BoardSpace;
import main.game.Pawn;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BoardSpaceTest {

    BoardSpace boardSpace = new BoardSpace(12, 50, BoardSpace.spaceType.normal, 45);
    ArrayList<Pawn> pawns;

    @Test
    void getxPosition() {
        assertEquals(12, boardSpace.getxPosition());
    }

    @Test
    void setxPosition() {
        BoardSpace bsm = new BoardSpace(12, 50, BoardSpace.spaceType.normal, 45);
        bsm.setxPosition(20);
    }

    @Test
    void getyPosition() {
        assertEquals(50, boardSpace.getyPosition());
    }

    @Test
    void setyPosition() {
    }

    @Test
    void getPawns() {
        boardSpace.setPawns(pawns);
        assertEquals(pawns, boardSpace.getPawns());
    }

    @Test
    void getSpace(){
        boardSpace.setSpaceType(BoardSpace.spaceType.blueSlide);
        assertEquals(BoardSpace.spaceType.blueSlide, boardSpace.getSpaceType());
        assertNotEquals(BoardSpace.spaceType.greenSlide, boardSpace.getSpaceType());
    }
}