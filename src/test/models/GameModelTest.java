package test.models;

import main.models.GameModel;
import org.junit.jupiter.api.Test;

import static main.game.Pawn.pawnColors;
import static org.junit.jupiter.api.Assertions.*;

class GameModelTest {

    @Test
    void createNewGame() {
        GameModel gameModel = new GameModel();
        boolean success = gameModel.createNewGame("[TEST] Created from GameModelTest.createNewGame()");
        assertEquals(true, success);
    }

    @Test
    void whoGoesFirst() {
        assertNotEquals(pawnColors.Blue, GameModel.whoGoesFirst());
        assertNotEquals(pawnColors.Red, GameModel.whoGoesFirst());
        assertNotEquals(pawnColors.Yellow, GameModel.whoGoesFirst());
        assertNotEquals(pawnColors.Green, GameModel.whoGoesFirst());
    }
    @Test
    void getGameStatus(){
        GameModel gameModel = new GameModel();
        gameModel.setGameStatus(GameModel.GameStatus.setting_up);
        assertEquals(GameModel.GameStatus.setting_up,gameModel.getGameStatus());
    }
    @Test
    void getGameName(){
        GameModel gameModel = new GameModel();
        gameModel.setGameName("name");
        assertEquals("name",gameModel.getGameName());
    }
    @Test
    void getNumOpponents(){
        GameModel gameModel = new GameModel();
        gameModel.setNumOpponents(3);
        assertEquals(3,gameModel.getNumOpponents());
    }

}