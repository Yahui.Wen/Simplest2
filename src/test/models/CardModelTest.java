package test.models;

import main.models.CardModel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CardModelTest {

    public static CardModel cardModel = new CardModel(0);

    @Test
    void isMoveFromStart() {
        CardModel cardModel = new CardModel(0);
        cardModel.setMoveFromStart(false);
        assertEquals(false, cardModel.isMoveFromStart());
    }

    @Test
    void setMoveFromStart() {
        cardModel.setCardNumber(4);
        assertEquals(4, cardModel.getCardNumber());
    }

    @Test
    void isDrawAgain() {
        CardModel cardModel = new CardModel(0);
        assertEquals(false, cardModel.isDrawAgain());
    }

    @Test
    void setDrawAgain() {
        cardModel.setDrawAgain(true);
        assertEquals(true, cardModel.isDrawAgain());
    }

    @Test
    void isSplitPawns() {
        CardModel cardModel = new CardModel(0);
        assertEquals(false, cardModel.isSplitPawns());
    }

    @Test
    void setSplitPawns() {
        cardModel.setSplitPawns(false);
        assertEquals(false, cardModel.isSplitPawns());
    }

    @Test
    void isPositionSwappable() {
        CardModel cardModel = new CardModel(0);
        assertEquals(true, cardModel.isPositionSwappable());
    }

    @Test
    void setPositionSwappable() {
        cardModel.setPositionSwappable(false);
        assertEquals(false, cardModel.isPositionSwappable());
    }

    @Test
    void getCardNumber() {
        CardModel cardModel = new CardModel(0);
        assertEquals(0, cardModel.getCardNumber());
    }

    @Test
    void setCardNumber() {
        cardModel.setCardNumber(7);
        assertEquals(7, cardModel.getCardNumber());
    }

    @Test
    void getForwardMoves() {
        CardModel cardModel = new CardModel(0);
        assertEquals(0, cardModel.getForwardMoves());
    }

    @Test
    void setForwardMoves() {
        cardModel.setForwardMoves(9);
        assertEquals(9, cardModel.getForwardMoves());
    }

    @Test
    void getReverseMoves() {
        CardModel cardModel = new CardModel(0);
        assertEquals(0, cardModel.getReverseMoves());
    }

    @Test
    void setReverseMoves() {
        cardModel.setReverseMoves(9);
        assertEquals(9, cardModel.getReverseMoves());
    }

}