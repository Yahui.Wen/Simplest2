package test.models;

import main.models.GameModel;
import main.game.Pawn;
import main.models.PlayerModel;
import org.junit.jupiter.api.Test;

import static main.models.PlayerModel.ComputerAbility.Smart;
import static main.models.PlayerModel.ComputerFeels.Mean;
import static main.models.PlayerModel.MoveQualityIndicators.Start;
import static org.junit.jupiter.api.Assertions.*;

class PlayerModelTest {

    @Test
    void testMoveQualityIndicatorHashMap() {
        double testing = PlayerModel.getMultiplier(Smart, Mean, Start);
        assertEquals(.05 * .1, testing);
    }


    @Test
    void testComputerPlayerConstructor() {
        GameModel game = new GameModel();
        boolean successfullyCreatedGame = game.createNewGame("[TEST] Created from PlayerModelTest.testComputerPlayerConstructor()");
        assertEquals(true, successfullyCreatedGame);
        if (!successfullyCreatedGame) {
            return;
        }
        PlayerModel player = new PlayerModel(Smart, PlayerModel.ComputerFeels.Nice, game);
        assertNotEquals(Pawn.pawnColors.Empty, player.getPawnColor());
        assertEquals(Smart, player.getAbility());
        assertEquals(PlayerModel.ComputerFeels.Nice, player.getFeels());
        assertEquals(game, player.getGame());
        assertNotEquals(0, player.getGamePlayerId());
        assertNotNull(player.getPawnColor());
        assertNotNull(player.getGamePlayerId());
    }

    @Test
    void testHumanPlayerConstructor() {
        GameModel game = new GameModel();
        boolean successfullyCreatedGame = game.createNewGame("[TEST] Created from PlayerModelTest.testHumanPlayerConstructor()");
        assertEquals(true, successfullyCreatedGame);
        if (!successfullyCreatedGame) {
            return;
        }
        PlayerModel player = new PlayerModel("[TEST] Created from PlayerModelTest.testHumanPlayerConstructor()", game, Pawn.pawnColors.Blue);
        assertEquals("[TEST] Created from PlayerModelTest.testHumanPlayerConstructor()", player.getPlayerName());
        assertEquals(Pawn.pawnColors.Blue, player.getPawnColor());
        assertEquals(game, player.getGame());
        assertNotEquals(0, player.getPlayerId());
        assertNotEquals(0, player.getGamePlayerId());
        assertNotNull(player.getPlayerId());
        assertNotNull(player.getGamePlayerId());
    }
    @Test
    void getAbility(){
        GameModel game = new GameModel();
        PlayerModel player = new PlayerModel("[TEST] Created from PlayerModelTest.testHumanPlayerConstructor()", game, Pawn.pawnColors.Blue);
        player.setAbility(Smart);
        assertEquals(Smart,player.getAbility());
        assertNotEquals(PlayerModel.ComputerAbility.Naive,player.getAbility());
    }
    @Test
    void getFeel(){
        GameModel game = new GameModel();
        PlayerModel player = new PlayerModel("[TEST] Created from PlayerModelTest.testHumanPlayerConstructor()", game, Pawn.pawnColors.Blue);
        player.setFeels(PlayerModel.ComputerFeels.Nice);
        assertEquals(PlayerModel.ComputerFeels.Nice,player.getFeels());
        assertNotEquals(Mean,player.getFeels());
    }
    @Test
    void getPlace(){
        GameModel game = new GameModel();
        PlayerModel player=new PlayerModel(Smart,PlayerModel.ComputerFeels.Nice,game);
        player.setPlace(1);
        assertEquals(1,player.getPlace());
        assertNotEquals(0,player.getPlace());
    }


}