package test.models;

import main.models.CardModel;
import main.models.DeckModel;
import main.models.GameModel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckModelTest {

    public static String normalDeck = "0,0,0,0,1,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,7,7,7,7,8,8,8,8,10,10,10,10,11,11,11,11,12,12,12,12";
    public static ArrayList<CardModel> cards;

    @Test
    void deckConstructor() {
        GameModel game = new GameModel();
        DeckModel deckModel = new DeckModel(game);
        assertEquals(45, deckModel.getCards().size());
    }

    @Test
    void cardReshuffle() {
        GameModel game = new GameModel();
        DeckModel deckModel = new DeckModel(game);
        deckModel.cardReshuffle();
        assertNotEquals(normalDeck, deckModel.toString());
    }

    @Test
    void drawFromDeck() {
        GameModel game = new GameModel();
        DeckModel deckModel = new DeckModel(game);
        int originalDeckSize = deckModel.getCards().size();
        CardModel originalCurrentCard = deckModel.getCurrentCard();
        deckModel.drawFromDeck();
        int newDeckSize = deckModel.getCards().size();
        CardModel newCurrentCard = deckModel.getCurrentCard();
        assertEquals(originalDeckSize - 1, newDeckSize);
        assertNotEquals(originalCurrentCard, newCurrentCard);
    }

    @Test
    void testToString() {
        GameModel game = new GameModel();
        DeckModel deckModel = new DeckModel(game);
        String asAString = deckModel.toString();
        assertEquals(normalDeck, asAString);
    }

    @Test
    void deckRefresh() {

    }
    @Test
    void getGame(){
        GameModel game = new GameModel();
        DeckModel deckModel = new DeckModel(game);
        deckModel.setGame(game);
        assertEquals(game,deckModel.getGame());
    }
    @Test
    void setCards(){
        GameModel game = new GameModel();
        DeckModel deckModel = new DeckModel(game);
        deckModel.setCards(cards);
        assertEquals(cards,deckModel.getCards());
    }


}