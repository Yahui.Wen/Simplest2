package main.game;

import java.util.ArrayList;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import main.game.BoardSpace.spaceType;

public class Board extends Pane {

    public static int boardWidth = 16;
    public static int boardHeight = 16;

    private ArrayList<BoardSpace> boardSpaces;

    public Board() {
        boardSpaces = new ArrayList<BoardSpace>();
        for (int h = 1; h <= boardHeight; h++) {
            for (int w = 1; w <= boardWidth; w++) {
                boardSpaces.add(new BoardSpace(w, h, spaceType.normal, 0));
            }
        }
        initializeSpaceIds();
        setPriorities();
    }

    public ArrayList<BoardSpace> getBoardSpaces() {
        return boardSpaces;
    }

    public void setBoardSpaces(ArrayList<BoardSpace> boardSpaces) {
        this.boardSpaces = boardSpaces;
    }

    public void setPriorities() {
        setTopPriorities();
        setRightPriorities();
        setBottomPriorities();
        setLeftPriorities();
    }

    public void setTopPriorities() {
        int priority = 10;
        for (int i = 0; i < 16; i++) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    public void setRightPriorities() {
        int priority = 170;
        for (int i = 31; i < 256; i += 16) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    public void setBottomPriorities() {
        int priority = 320;
        for (int i = 254; i >= 240; i--) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    public void setLeftPriorities() {
        int priority = 470;
        for (int i = 224; i >= 16; i -= 16) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    //Sets the spaceType for start spaces, home spaces, slide spaces.
    //**(Note: This does not differentiate between the start of slides and end of slides)**
    public void initializeSpaceIds() {
        initializeHomeAndSafetySpaceIds();
        initializeStartSpaceIds();
        initializeSliderSpaceIds();
    }

    private void initializeHomeAndSafetySpaceIds() {
        //initialize Blue home spaces
        int[] blueHome = {18, 34, 50, 66, 82, 98};
        //int blueSafetyIndex = 98;
        int blueHomePriority = 31;
        for (int homeSpaceIndex : blueHome) {
            if (homeSpaceIndex == 98) {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.blueHome);
            } else {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.blueSafety);
            }
            boardSpaces.get(homeSpaceIndex).setPriority(blueHomePriority);
            blueHomePriority++;
        }

        //initialize Yellow home spaces
        int yellowHomeSpaceStart = 46;
        int yellowHomeSpaceEnd = 41;
        int yellowHomePriority = 181;
        for (int i = yellowHomeSpaceStart; i >= yellowHomeSpaceEnd; i--) {
            if (i == yellowHomeSpaceEnd) {
                boardSpaces.get(i).setSpaceType(spaceType.yellowHome);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.yellowSafety);
            }
            boardSpaces.get(i).setPriority(yellowHomePriority);
            yellowHomePriority++;
        }

        //initialize Green home spaces
        int[] greenHome = {237, 221, 205, 189, 173, 157};
        int greenHomePriority = 331;
        for (int homeSpaceIndex : greenHome) {
            if (homeSpaceIndex == 157) {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.greenHome);
            } else {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.greenSafety);
            }
            boardSpaces.get(homeSpaceIndex).setPriority(greenHomePriority);
            greenHomePriority++;
        }

        //initialize Red home spaces
        int redHomeSpaceStart = 209;
        int redHomeSpaceEnd = 214;
        int redHomePriority = 471;
        for (int i = redHomeSpaceStart; i <= redHomeSpaceEnd; i++) {
            if (i == redHomeSpaceEnd) {
                boardSpaces.get(i).setSpaceType(spaceType.redHome);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.redSafety);
            }
            boardSpaces.get(i).setPriority(redHomePriority);
            redHomePriority++;
        }
    }

    private void initializeStartSpaceIds() {
        //initialize Red start space
        int redStartIndex = 177;
        boardSpaces.get(redStartIndex).setSpaceType(spaceType.redStart);
        boardSpaces.get(redStartIndex).setPriority(499);

        //initialize Green start space
        int greenStartIndex = 235;
        boardSpaces.get(greenStartIndex).setSpaceType(spaceType.greenStart);
        boardSpaces.get(greenStartIndex).setPriority(349);

        //initialize Yellow start space
        int yellowStartIndex = 78;
        boardSpaces.get(yellowStartIndex).setSpaceType(spaceType.yellowStart);
        boardSpaces.get(yellowStartIndex).setPriority(199);

        //initialize Blue start space
        int blueStartIndex = 20;
        boardSpaces.get(blueStartIndex).setSpaceType(spaceType.blueStart);
        boardSpaces.get(blueStartIndex).setPriority(49);
    }

    private void initializeSliderSpaceIds() {
        //initialize Red slide spaces
        int[] redSlide1 = {224, 208, 192, 176};
        for (int slideSpaceIndex : redSlide1) {
            boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redSlide);
        }
        int[] redSlide2 = {96, 80, 64, 48, 32};
        for (int slideSpaceIndex : redSlide2) {
            boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redSlide);
        }

        //initialize Green slide spaces
        int greenSlide1StartIndex = 254;
        int greenSlide1EndIndex = 251;
        for (int i = greenSlide1StartIndex; i >= greenSlide1EndIndex; i--) {
            boardSpaces.get(i).setSpaceType(spaceType.greenSlide);
        }
        int greenSlide2StartIndex = 246;
        int greenSlide2EndIndex = 242;
        for (int i = greenSlide2StartIndex; i >= greenSlide2EndIndex; i--) {
            boardSpaces.get(i).setSpaceType(spaceType.greenSlide);
        }

        //initialize Yellow slide spaces
        int[] yellowSlide1 = {31, 47, 63, 79};
        for (int slideSpaceIndex : yellowSlide1) {
            boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowSlide);
        }
        int[] yellowSlide2 = {159, 175, 191, 207, 223};
        for (int slideSpaceIndex : yellowSlide2) {
            boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowSlide);
        }

        //initialize Blue slide spaces
        int blueSlide1StartIndex = 1;
        int blueSlide1EndIndex = 4;
        for (int i = blueSlide1StartIndex; i <= blueSlide1EndIndex; i++) {
            boardSpaces.get(i).setSpaceType(spaceType.blueSlide);
        }
        int blueSlide2StartIndex = 9;
        int blueSlide2EndIndex = 13;
        for (int i = blueSlide2StartIndex; i <= blueSlide2EndIndex; i++) {
            boardSpaces.get(i).setSpaceType(spaceType.blueSlide);
        }
    }
    
    public void addBoardSpacesToGrid(GridPane grid) {
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            BoardSpace space = getBoardSpaces().get(i);
            switch (space.getSpaceType()) {
                case blueSafety:
                case blueSlide:
//                    space.setStyle("-fx-background-color: rgba(0,0,255,.5);");
                    break;
                case blueHome:
                case blueStart:
                    space.setStyle("-fx-background-color: rgba(0,0,255, 1);");
                    break;
                case redSafety:
                case redSlide:
//                    space.setStyle("-fx-background-color: rgba(255,0,0,.5);");
                    break;
                case redHome:
                case redStart:
                    space.setStyle("-fx-background-color: rgba(255,0,0,1);");
                    break;
                case yellowSafety:
                case yellowSlide:
//                    space.setStyle("-fx-background-color: rgba(255,255,0,.5);");
                    break;
                case yellowHome:
                case yellowStart:
                    space.setStyle("-fx-background-color: rgba(255,255,0,1);");
                    break;
                case greenSafety:
                case greenSlide:
//                    space.setStyle("-fx-background-color: rgba(0,255,0,.5);");
                    break;
                case greenHome:
                case greenStart:
                    space.setStyle("-fx-background-color: rgba(0,255,0,1);");
                    break;
            }
            grid.add(space, i % 16, i / 16);
        }
    }

    public void resize(GridPane grid) {
        double gridHeight = grid.getHeight();
        double cellHeight = gridHeight / 16;
        doResizing(cellHeight);
    }

    public void resize(GridPane grid, double windowHeight) {
        double estimatedCellSize = (windowHeight - 80) / 16;
        doResizing(estimatedCellSize);
    }

    private void doResizing(double cellHeight) {
        double defaultSpaceDimensions = cellHeight;
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            double newSpaceDimension = defaultSpaceDimensions;
            BoardSpace space = getBoardSpaces().get(i);
            switch (space.getSpaceType()) {
                case blueStart:
                case blueHome:
                case redStart:
                case redHome:
                case yellowStart:
                case yellowHome:
                case greenStart:
                case greenHome:
                    // TODO: 4/18/18 make the spaces bigger, and figure out how to position them.
                    // newSpaceDimension *= 2;
                    break;
            }
            space.setMaxHeight(newSpaceDimension);
            space.setMaxWidth(newSpaceDimension);
            space.setMinHeight(newSpaceDimension);
            space.setMinWidth(newSpaceDimension);
        }
        sizePawns(defaultSpaceDimensions);
    }

    public void sizePawns(double spaceDimension) {
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            for (int j = 0; j < getBoardSpaces().get(i).getPawns().size(); j++) {
                Pawn pawn = getBoardSpaces().get(i).getPawns().get(j);
                pawn.setRadius(spaceDimension / 3);
            }
        }
    }


    public void draw(GridPane grid) {
        addBoardSpacesToGrid(grid);
    }

    public void addPawnToBoard(Pawn pawn) {
        int pawnX = pawn.getX();
        int pawnY = pawn.getY();
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            BoardSpace space = getBoardSpaces().get(i);
            if (space.getxPosition() == pawnX && space.getyPosition() == pawnY) {
                space.addPawn(pawn);
                break;
            }
        }
    }

}
