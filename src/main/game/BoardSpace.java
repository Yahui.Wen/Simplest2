package main.game;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import java.util.ArrayList;

public class BoardSpace extends Pane {

    //**(Note: Need to add additional values for the start/end of the slides)**
    public enum spaceType {blueStart, yellowStart, greenStart, redStart, blueHome, yellowHome, greenHome, redHome,
                            blueSlide, yellowSlide, greenSlide, redSlide, blueSafety, yellowSafety, greenSafety, redSafety, normal}

    private int xPosition;
    private int yPosition;
    private BoardSpace.spaceType spaceType;
    private int priority;

    private ArrayList<Pawn> pawns = new ArrayList<>();

    public BoardSpace(int x, int y, BoardSpace.spaceType type, int priority) {
        super();
        setxPosition(x);
        setyPosition(y);
        setSpaceType(type);
        setPriority(priority);
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public BoardSpace.spaceType getSpaceType() {
        return spaceType;
    }

    public void setSpaceType(BoardSpace.spaceType spaceType) {
        this.spaceType = spaceType;
    }

    public ArrayList<Pawn> getPawns() {
        return pawns;
    }

    public void setPawns(ArrayList<Pawn> pawns) {
        this.pawns = pawns;
    }

    public void addPawn(Pawn pawn) {
        // TODO: 4/19/18 This might be were we add rules about pawns per space. 
        pawns.add(pawn);
        pawn.setBoardSpace(this);
        // TODO: 4/19/18 We don't want centered pawns on home or end spaces. 
        pawn.layoutXProperty().bind(this.widthProperty().divide(2));
        pawn.layoutYProperty().bind(this.heightProperty().divide(2));
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
