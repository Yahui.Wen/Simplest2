package main.game;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import main.models.PlayerModel;


public class Pawn extends Circle {
    /**
     * Pretty self explanatory... 'Empty' is used by the Sorry! API if for some reason the user
     * is able to try and create more than 4 players, the pawn color will be 'Empty' signifying
     * that there are no colors left to choose from and they cannot do what they are trying to do.
     */
    public enum pawnColors {
        Red, Blue, Green, Yellow, Empty
    }


    private int pawnId;
    private int gamePlayerId;
    private pawnColors color;
    private int x;
    private int y;

    /**
     * These are more to keep track of the objects that this pawn is attatched to. It will allow us to
     * implement a move methods, and know which board we need to look at, and what board space we need
     * to remove the pawn from.
     */
    private Board board;
    private BoardSpace boardSpace;


    /**
     * The X and Y positions (on a 16x16 grid... 1-16) of the starts will never change. We keep these in a static
     * context because they don't depend on anything, but will act as a lookup table when
     * creating new pawns of different colors.
     */
    private static final int redStartX = 2;
    private static final int redStartY = 12;
    private static final int blueStartX = 5;
    private static final int blueStartY = 2;
    private static final int greenStartX = 12;
    private static final int greenStartY = 15;
    private static final int yellowStartX = 15;
    private static final int yellowStartY = 5;

    /**
     * When create a new game, all the pawns will need to be created (not fetched form the database). This
     * means that all pawns will be in there start positions, and we only know of the game player that we
     * need to attach them to. Once we create the pawn, we can assign it it's id. From the player we can
     * get all the information we need to create the pawn correctly.
     *
     * @param player The player the pawn belongs to.
     */
    public Pawn(PlayerModel player, Board board) {
        setColor(player.getPawnColor());
        setGamePlayerId(player.getGamePlayerId());
        switch (getColor()) {
            case Red:
                setX(redStartX);
                setY(redStartY);
                break;
            case Blue:
                setX(blueStartX);
                setY(blueStartY);
                break;
            case Green:
                setX(greenStartX);
                setY(greenStartY);
                break;
            case Yellow:
                setX(yellowStartX);
                setY(yellowStartY);
                break;
        }
        setBoard(board);
        setStyle("-fx-background-color: black");
        EventHandler<MouseEvent> onClick = e -> {
            nextPlace();
        };
        addEventHandler(MouseEvent.MOUSE_CLICKED, onClick);
    }

    private void nextPlace() {
        System.out.println("Moving to the next space.");
        int currentSpacePriority = getBoardSpace().getPriority();
        BoardSpace currentCandidateSpace = getBoard().getBoardSpaces().get(0);
        int currentCandidateDifference = 999999; // Arbitrarily large difference.
        for (int i = 0; i < getBoard().getBoardSpaces().size(); i++) {
            BoardSpace space = getBoard().getBoardSpaces().get(i);
            if (space.getPriority() == 0) {
                continue;
            }
            int difference = Math.abs(space.getPriority() - currentSpacePriority);
            System.out.println(difference + " compare to " + currentCandidateDifference);
            if (space.getPriority() > currentSpacePriority && difference < currentCandidateDifference) {
                System.out.println("NEW MIN");
                currentCandidateSpace = space;
                currentCandidateDifference = difference;
            }
        }
        moveToBoardSpace(currentCandidateSpace);
    }


    public int getPawnId() {
        return pawnId;
    }

    public void setPawnId(int pawnId) {
        this.pawnId = pawnId;
    }

    public int getGamePlayerId() {
        return gamePlayerId;
    }

    public void setGamePlayerId(int gamePlayerId) {
        this.gamePlayerId = gamePlayerId;
    }

    public pawnColors getColor() {
        return color;
    }

    public void setColor(pawnColors color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public BoardSpace getBoardSpace() {
        return boardSpace;
    }

    public void setBoardSpace(BoardSpace boardSpace) {
        // TODO: 4/19/18 We will update the pawn position in the DB here.
        this.boardSpace = boardSpace;
        boardSpace.getChildren().add(this);
    }

    public void moveToBoardSpace(BoardSpace moveToBoardSpace) {
        getBoardSpace().getChildren().remove(this);
        setBoardSpace(moveToBoardSpace);
    }
}
