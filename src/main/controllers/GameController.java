package main.controllers;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.*;
import main.Sorry;
import main.game.Board;
import main.game.BoardSpace;

import java.io.IOException;

public class GameController extends ApplicationController {

    @FXML public GridPane gameGrid;
    @FXML public SplitPane splitPane;
    @FXML public AnchorPane rightPane;
    @FXML public AnchorPane leftPane;

    public Board board = new Board();

    @FXML
    public void initialize() {
        // Bind the table to always fill available space
        splitPane.prefWidthProperty().bind(mainPane.widthProperty());
        splitPane.prefHeightProperty().bind(mainPane.heightProperty());

        // When the window changes size, do this again.
        ChangeListener<Number> stageSizeListener = (observable, oldValue, newValue) -> {
            forceGameBoardToSqaureAndResizeBoard();
        };
        window.widthProperty().addListener(stageSizeListener);
        window.heightProperty().addListener(stageSizeListener);

        board.draw(gameGrid);

        Sorry.theGame.initializePawnsInStart();
        Sorry.theGame.addPawnsToBoard(board);

        forceGameBoardToSquareAndResizeBoard(ApplicationController.contentHeight);
    }

    //Access a specific cell within the gridPane
    private Pane getPaneFromGridPane(GridPane gridPane, int col, int row) {
        for (Node pane : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(pane) == col && GridPane.getRowIndex(pane) == row) {
                return (Pane) pane;
            }
        }
        return null;
    }

    public void forceGameBoardToSqaureAndResizeBoard() {
        forceGameBoardToSquareAndResizeBoard(rightPane.getHeight());
        board.resize(gameGrid);
    }

    public void forceGameBoardToSquareAndResizeBoard(double estimateHeight) {
        rightPane.setMinWidth(estimateHeight);
        rightPane.setMaxWidth(estimateHeight);
        // Size the pawns as well...
        board.sizePawns(estimateHeight / 16);
    }

    @Override
    public String getViewPath() {
        return "/main/views/game.fxml";
    }
}
