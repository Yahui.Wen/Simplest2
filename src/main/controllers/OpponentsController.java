package main.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;
import main.Sorry;

import java.io.IOException;

public class OpponentsController extends ApplicationController {

    @FXML ToggleButton opp1;
    @FXML ToggleButton opp2;
    @FXML ToggleButton opp3;

    public void goToOpponentSetup() throws IOException {
        if (opp1.isSelected()){
            Sorry.theGame.setNumOpponents(1);
        } else if (opp2.isSelected()) {
            Sorry.theGame.setNumOpponents(2);
        } else if (opp3.isSelected()){
            Sorry.theGame.setNumOpponents(3);
        }
        changeView(new OpponentSetupController());
    }

    @Override
    public String getViewPath() {
        return "/main/views/opponents.fxml";
    }
}