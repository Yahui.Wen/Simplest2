package main.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import main.Sorry;
import main.controllers.components.footerController;
import main.controllers.components.menubarController;

import java.io.IOException;

import static java.lang.System.exit;


public abstract class ApplicationController implements Controller{

    public static int defaultHeight = 800;
    public static int defaultWidth = 1200;
    public static int contentHeight = defaultHeight - 70;

    public Stage window;

    @FXML public Pane mainPane;
    @FXML public menubarController menubarController;
    @FXML public footerController footerController;

    public ApplicationController() {
        window = Sorry.applicationWindow;
    }

    public void changeView(ApplicationController controller) throws IOException {
        System.out.println("View: " + controller.getViewPath());
        Parent computerSelect = FXMLLoader.load(getClass().getResource(controller.getViewPath()));
        computerSelect.getStylesheets().add("/main/resources/styles/global.css");
        window.setScene(new Scene(computerSelect, ApplicationController.defaultWidth, ApplicationController.defaultHeight));
        window.setMinWidth(1050);
        window.setMinHeight(650);
        window.show();
    }

    public void closeGame() {
        System.out.println("Goodbye.");
        exit(0);
    }

    public void goToLeaderboard() throws IOException {
        changeView(new LeaderBoardController());
    }

    public void gotoResumeGame() throws  IOException {
        changeView(new NewGameController());
    }

    public void gotoCreateNewGame() throws IOException {
        changeView(new NewGameController());
    }

    public void gotoMainMenu() throws IOException {
        changeView(new MainMenuController());
    }

    public void gotoStatsPage() throws  IOException {
        changeView(new GameStatsController());
    }

    public void showWarning(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText(null);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.showAndWait();
    }

}
