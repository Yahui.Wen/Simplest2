package main.controllers.components;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import main.controllers.ApplicationController;

public class footerController extends ApplicationController {

    @FXML public Label footerText;
    @FXML public Label footerCopyright;

    @Override
    public String getViewPath() {
        return null;
    }

}
