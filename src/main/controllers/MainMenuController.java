package main.controllers;

import javafx.fxml.FXML;

import java.io.IOException;

public class MainMenuController extends ApplicationController {

    @FXML
    public void initialize() {
        // Update the footer to show the page name
        footerController.footerText.setText("Main Menu");
    }

    @Override
    public String getViewPath() {
        return "/main/views/main_menu.fxml";
    }

}