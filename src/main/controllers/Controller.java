package main.controllers;

public interface Controller {

    String getViewPath();

}
