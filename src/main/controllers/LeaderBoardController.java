package main.controllers;

import com.google.gson.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import main.helpers.LeaderBoardRow;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;

import java.util.Map;
import java.util.Set;

public class LeaderBoardController extends ApplicationController {

    @FXML public TableView<LeaderBoardRow> leaderBoardTable;
    @FXML public TableColumn<LeaderBoardRow, Integer> placeColumn;
    @FXML public TableColumn<LeaderBoardRow, String> playerNameColumn;

    @FXML
    public void initialize() {
        // Update the footer to show the page name
        footerController.footerText.setText("Leader Board");

        // Bind the table to always fill available space
        leaderBoardTable.prefWidthProperty().bind(mainPane.widthProperty());
        leaderBoardTable.prefHeightProperty().bind(mainPane.heightProperty());

        // Finish initializing the table columns
        placeColumn.setCellValueFactory(new PropertyValueFactory<>("place"));
        playerNameColumn.setCellValueFactory(new PropertyValueFactory<>("playerName"));

        // Populate the table with data
        leaderBoardTable.setItems(getLeaderboardData());
    }

    @Override
    public String getViewPath() {
        return "/main/views/leader_board.fxml";
    }

    public ObservableList<LeaderBoardRow> getLeaderboardData() {
        ObservableList<LeaderBoardRow> data = FXCollections.observableArrayList();
        SorryAPIResponse response = SorryAPI.getAllGames();
        if (response.isSuccess() && !response.getData().isJsonNull()) {
            Set<Map.Entry<String, JsonElement>> entrySet = response.getData().entrySet();
            for(Map.Entry<String,JsonElement> entry : entrySet){
                JsonObject record = new JsonParser().parse(entry.getValue().toString()).getAsJsonObject();
                int gameId = record.get("pmk_game_id").getAsInt();
                String gameName = record.get("fld_game_name").getAsString();
                data.add(new LeaderBoardRow(gameId, gameName));
            }
        }
        return data;
    }

}


