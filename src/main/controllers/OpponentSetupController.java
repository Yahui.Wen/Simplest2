package main.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import main.Sorry;
import main.game.Pawn;
import main.models.*;

import java.awt.*;
import java.io.IOException;

public class OpponentSetupController extends ApplicationController {

    @FXML public VBox opponentList;
    @FXML public Button letsPlayButton;

    private ObservableList<PlayerModel.ComputerAbility> availableAbilities = FXCollections.observableArrayList();
    private ObservableList<PlayerModel.ComputerFeels> availableFeels = FXCollections.observableArrayList();

    private ObservableList<ChoiceBox[]> opponentConfigurations = FXCollections.observableArrayList();
    private static final int abilityIndex = 0;
    private static final int feelsIndex = 1;

    public OpponentSetupController() {
        for (PlayerModel.ComputerAbility ability : PlayerModel.ComputerAbility.values()) {
            PlayerModel.ComputerAbility option = ability;
            availableAbilities.add(option);
        }
        for (PlayerModel.ComputerFeels feels : PlayerModel.ComputerFeels.values()) {
            PlayerModel.ComputerFeels option = feels;
            availableFeels.add(option);
        }
    }

    @FXML
    public void initialize() {
        for (int i = 1; i <= Sorry.theGame.getNumOpponents(); i++) {
            addOpponentOptionConfigurationRow(i);
        }
        opponentList.getChildren().add(letsPlayButton);
    }

    public void addOpponentOptionConfigurationRow(int number) {
        HBox singleOpponentContainer = new HBox();

        Label label = new Label("Computer " + number);
        ChoiceBox abilityChoiceBox = new ChoiceBox(availableAbilities);
        ChoiceBox feelsChoiceBox = new ChoiceBox(availableFeels);

        abilityChoiceBox.getSelectionModel().selectFirst();
        feelsChoiceBox.getSelectionModel().selectFirst();

        // Add to the property so we can access value later...
        ChoiceBox[] playerConfig = {abilityChoiceBox, feelsChoiceBox};
        opponentConfigurations.add(playerConfig);

        singleOpponentContainer.getChildren().addAll(label, abilityChoiceBox, feelsChoiceBox);
        opponentList.getChildren().add(singleOpponentContainer);
    }

    public void letsPlayButtonPressed() throws IOException {
        for (int i = 0; i < opponentConfigurations.size(); i++) {
            ChoiceBox[] singlePlayerConfigContainer = opponentConfigurations.get(i);
            ChoiceBox abilityChoiceBox = singlePlayerConfigContainer[abilityIndex];
            ChoiceBox feelsChoiceBox = singlePlayerConfigContainer[feelsIndex];
            String ability = abilityChoiceBox.getSelectionModel().getSelectedItem().toString();
            String feels = feelsChoiceBox.getSelectionModel().getSelectedItem().toString();
            PlayerModel computerPlayer = new PlayerModel(PlayerModel.ComputerAbility.valueOf(ability), PlayerModel.ComputerFeels.valueOf(feels), Sorry.theGame);
            Sorry.theGame.addPlayer(computerPlayer);
        }
        changeView(new GameController());
    }

    @Override
    public String getViewPath() {
        return "/main/views/opponent_setup.fxml";
    }
}
