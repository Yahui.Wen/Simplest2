package main.controllers;

import javafx.fxml.FXML;

public class GameStatsController extends ApplicationController {

    @FXML
    public void initialize() {
        // Update the footer to show the page name
        footerController.footerText.setText("Game Stats");
    }


    @Override
    public String getViewPath() {
        return "/main/views/game_stats.fxml";
    }

}
