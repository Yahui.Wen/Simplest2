package main.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import main.Sorry;
import main.models.GameModel;
import main.game.Pawn;
import javafx.scene.control.TextField;
import main.models.PlayerModel;

import java.io.IOException;


public class NewGameController extends ApplicationController {

    @FXML private TextField gameName;
    @FXML private TextField username;
    @FXML public ChoiceBox colorChoice;

    public ObservableList<Pawn.pawnColors> colorList = FXCollections.observableArrayList();
    private Pawn.pawnColors pawnColor;

    public NewGameController() {
        Sorry.theGame = new GameModel();
    }

    @FXML
    public void initialize(){
        for (Pawn.pawnColors color : Pawn.pawnColors.values()) {
            Pawn.pawnColors option = color;
            colorList.add(option);
        }
        colorChoice.setValue(colorList.get(0));
        colorChoice.setItems(colorList);
    }

    public boolean validation(){
        if (username.getText().isEmpty() || gameName.getText().isEmpty()) {
            showWarning("Validate fields", "Please enter your game name");
            return false;
        } else {
            return true;
        }
    }

    public void createNew() throws IOException {
        if (!validation()) {
            return;
        }
        if (!Sorry.theGame.createNewGame(gameName.getText())) {
           showWarning("Uh oh.", "Something went wrong. Please try again later.");
           return;
        }
        pawnColor = (Pawn.pawnColors) colorChoice.getValue();
        PlayerModel player = new PlayerModel(username.getText(), Sorry.theGame, pawnColor);
        Sorry.theGame.addPlayer(player);
        changeView(new OpponentsController());
    }

    @Override
    public String getViewPath() {
        return "/main/views/new_game.fxml";
    }

}

