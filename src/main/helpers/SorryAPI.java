package main.helpers;

import com.google.gson.*;
import main.models.DeckModel;
import main.models.GameModel;
import main.game.Pawn;
import main.models.PlayerModel;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * The SorryAPI implements interaction with the Sorry! API hosted on UVM's server.
 * Built into the class are methods to interact with each of the endpoints that
 * will give a useful response.
 *
 * @author Andrew Hollar, Ryan Berliner, Sam Nguon, Yahui Wen
 */
public class SorryAPI {
    /**
     * Holds all of the valid endpoints that the Sorry! API accepts. When implementing new
     * methods to new endpoints within this class, always use an Endpoint enum.
     */
    public enum Endpoint {
        verify_connection, get_all_games, create_game, save_game_deck, create_human_player, create_computer_player,
        create_pawn, update_pawn_position
    }

    /**
     * The location of the Sorry! API file on UVM's server.
     */
    public static final String URL = "https://rberline.w3.uvm.edu/sorry/api/";

    /**
     * When we send a request to the Sorry! API, we should to tell it what kind of 'device/browser' it
     * is coming from. This is an arbitrary name that we just made up.
     */
    public static final String USER_AGENT = "Sorry/Java";

    /**
     * Like all API's, credentials are a good idea. This is the location of the .txt file that holds
     * the credentials. That file should never be checked in to version control, and the credentials
     * should never be hard coded in this class.
     */
    public static final String CREDENTIALS_FILE = "sorry_api_credentials.txt";

    /**
     * Updates a pawn entry with new x and y positions.
     *
     * @param pawnId The id of the pawn (this is the database id)
     * @param positionX The new x position of the pawn.
     * @param positionY The new y position of the pawn.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse updatePawnPosition(int pawnId, int positionX, int positionY) {
        String query = "pawn_id=" + pawnId + "&position_x=" + positionX + "&position_y=" + positionY;
        SorryAPIResponse response = sendRequest(Endpoint.update_pawn_position, query);
        return response;
    }

    /**
     * Creates a pawn entry.
     *
     * @param gamePlayerId The game player id (this is the database id)
     * @param positionX The x position of the new pawn.
     * @param positionY The y position of the new pawn.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse createPawn(int gamePlayerId, int positionX, int positionY) {
        String query = "game_player_id=" + gamePlayerId + "&position_x=" + positionX + "&position_y=" + positionY;
        SorryAPIResponse response = sendRequest(Endpoint.create_pawn, query);
        return response;
    }

    /**
     * Creates a computer player entry.
     *
     * @param game The game object, this will be used to get the game id (this is the database id)
     * @param ability The ability of the computer player (smart vs. naive)
     * @param feels How the computer player is feeling (mean vs. nice)
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse createComputerPlayer(
            GameModel game,
            PlayerModel.ComputerAbility ability,
            PlayerModel.ComputerFeels feels) {
        int gameId = game.getGameId();
        String query = "game_id=" + gameId + "&ability=" + ability.toString() + "&feels=" + feels.toString();
        SorryAPIResponse response = sendRequest(Endpoint.create_computer_player, query);
        return response;
    }

    /**
     * Creates a human player entry.
     *
     * @param playerName The name of the player.
     * @param game The game object to attach the player to.
     * @param pawnColor The color of the pawn for the player.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse createHumanPlayer(String playerName, GameModel game, Pawn.pawnColors pawnColor) {
        int gameId = game.getGameId();
        String query = "game_id=" + gameId + "&player_name=" + playerName + "&pawn_color=" + pawnColor;
        SorryAPIResponse response = sendRequest(Endpoint.create_human_player, query);
        return response;
    }

    /**
     * Save the current state of the game deck.
     *
     * @param gameId The id of the game.
     * @param deck The deck object.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse saveGameDeck(int gameId, DeckModel deck) {
        String stringDeck = deck.toString();
        SorryAPIResponse response = sendRequest(Endpoint.save_game_deck, "game_id=" + gameId + "&deck_string=" + stringDeck);
        return response;
    }

    /**
     * Creates a game.
     *
     * @param gameName The name of the game.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse createGame(String gameName) {
        SorryAPIResponse response = sendRequest(Endpoint.create_game, "name=" + gameName);
        return response;
    }

    /**
     * Fetches all games.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse getAllGames() {
        SorryAPIResponse response = sendRequest(Endpoint.get_all_games, null);
        return response;
    }

    /**
     * A helper 'test' method to verify that connection to the Sorry! API works.
     *
     * @return Everything you need to know about the request.
     * @see    SorryAPIResponse
     */
    public static SorryAPIResponse verifyConnection() {
        SorryAPIResponse response = sendRequest(Endpoint.verify_connection, null);
        return response;
    }

    /**
     * Try's to retrieve the API key from the credentials file.
     *
     * @return String This gives you the api key, or the error code (which is a camel case message).
     */
    private static String getAPIKey() {
        String apiKey = "invalid_key";
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(CREDENTIALS_FILE));
        } catch (FileNotFoundException e) {
            return "no_credentials_file";
        }
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            apiKey = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                apiKey = "buffer_error";
            }
        }
        return apiKey.trim();
    }

    /**
     * This provides the core functionality for sending requests to the Sorry! API. It will return
     * a SorryAPIResponse object that is populated with data, or the information about what went wrong.
     *
     * @param endpoint The endpoint you'd like to hit on the API.
     * @param data     A query string of data you'd like to send along. ex. "var1=dog&var2=cat"
     *
     * @return         Everything you need to know about the request.
     * @see            SorryAPIResponse
     */
    private static SorryAPIResponse sendRequest(Endpoint endpoint, String data) {
        SorryAPIResponse sorryAPIResponse = new SorryAPIResponse();
        try {
            // Create the full query string to send along with the request.
            String fullData = "api_endpoint=" + endpoint.toString() + "&api_key=" + getAPIKey() + "&" + data;
            // Create the URL object, and then create and send the request with it.
            URL obj = new URL(URL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(fullData.getBytes());
            os.flush();
            os.close();
            // Now that we have the response, let's fill out the sorryAPIResponse appropriately.
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JsonElement jsonElement = new JsonParser().parse(response.toString());
                return new SorryAPIResponse(jsonElement);
            } else {
                sorryAPIResponse.setMessage("API failed with response code: " + responseCode);
            }
        } catch (IOException ex) {
            sorryAPIResponse.setMessage("Unable to send request to API.");
        }
        return sorryAPIResponse;
    }
}
