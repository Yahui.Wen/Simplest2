package main.models;

import main.game.Pawn;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static main.models.PlayerModel.ComputerAbility.Naive;
import static main.models.PlayerModel.ComputerAbility.Smart;
import static main.models.PlayerModel.MoveQualityIndicators.Start;

public class PlayerModel extends ApplicationModel {

    public enum ComputerAbility {
        Smart, Naive
    }

    public enum ComputerFeels {
        Mean, Nice
    }

    public enum MoveQualityIndicators {
        Start, Finish, Safety, Displace, Progress, Defense
    }

    /**
     * These multipliers give weight to difference possible moves a computer player will make.
     * The names purposefully contain the .toString() of the ComputerAbility, ComputerFeels
     * and MoveQualityInidicators enums so we can write smart functions for accessing them.
     */
    public static Map<String, Double> abilityMultipliers = new HashMap<>();
    public static Map<String, Double> feelsMultiplier = new HashMap<>();
    static {
        abilityMultipliers.put("SmartStart", 0.1);
        abilityMultipliers.put("NaiveStart", 0.025);
        abilityMultipliers.put("SmartFinish", 0.02);
        abilityMultipliers.put("NaiveFinish", 0.7);
        abilityMultipliers.put("SmartSafety", 0.1);
        abilityMultipliers.put("NaiveSafety", 0.025);
        abilityMultipliers.put("SmartDisplace", 0.0);
        abilityMultipliers.put("NaiveDisplace", 0.025);
        abilityMultipliers.put("SmartProgress", 0.39);
        abilityMultipliers.put("NaiveProgress", 0.2);
        abilityMultipliers.put("SmartDefense", 0.39);
        abilityMultipliers.put("NaiveDefense", 0.025);
        feelsMultiplier.put("MeanStart", 0.05);
        feelsMultiplier.put("NiceStart", 0.02);
        feelsMultiplier.put("MeanFinish", 0.05);
        feelsMultiplier.put("NiceFinish", 0.2);
        feelsMultiplier.put("MeanSafety", 0.1);
        feelsMultiplier.put("NiceSafety", 0.02);
        feelsMultiplier.put("MeanDisplace", 0.45);
        feelsMultiplier.put("NiceDisplace", 0.0);
        feelsMultiplier.put("MeanProgress", 0.05);
        feelsMultiplier.put("NiceProgress", 0.2);
        feelsMultiplier.put("MeanDefense", 0.3);
        feelsMultiplier.put("NiceDefense", 0.2);
    }

    public static double getMultiplier(ComputerAbility ability, ComputerFeels feels, MoveQualityIndicators indicator) {
        double abilityMultiplierValue = abilityMultipliers.get(ability.toString() + indicator.toString());
        double feelsMultiplierValue = feelsMultiplier.get(feels.toString() + indicator.toString());
        return abilityMultiplierValue * feelsMultiplierValue;
    }

    private GameModel game;
    private int playerId;
    private int gamePlayerId;
    private String playerName;
    private ComputerAbility ability;
    private ComputerFeels feels;
    private int place;
    private Pawn.pawnColors pawnColor;

    public PlayerModel(String playerName, GameModel game, Pawn.pawnColors pawnColor) {
        // This is for creating a human opponent.
        // TODO: 4/10/18 Check username in db, only create new player if it doesn't already exist.
        SorryAPIResponse response = SorryAPI.createHumanPlayer(playerName, game, pawnColor);
        if (response.isSuccess()) {
            setPlayerName(playerName);
            setGame(game);
            setPawnColor(pawnColor);
            setPlayerId(response.getData().get("player_id").getAsInt());
            setGamePlayerId(response.getData().get("game_player_id").getAsInt());
        }
    }

    public PlayerModel(ComputerAbility ability, ComputerFeels feels, GameModel game) {
        // This is for creating a computer opponent.
        SorryAPIResponse response = SorryAPI.createComputerPlayer(game, ability, feels);
        if (response.isSuccess()) {
            String colorFromDB = response.getData().get("pawn_color").getAsString();
            setPawnColor(Pawn.pawnColors.valueOf(colorFromDB));
            setAbility(ability);
            setFeels(feels);
            setGamePlayerId(response.getData().get("game_player_id").getAsInt());
            setGame(game);
        } else {
            System.out.println(response.getMessage());
        }
    }

    public GameModel getGame() {
        return game;
    }

    public void setGame(GameModel game) {
        this.game = game;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getGamePlayerId() {
        return gamePlayerId;
    }

    public void setGamePlayerId(int gamePlayerId) {
        this.gamePlayerId = gamePlayerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public ComputerAbility getAbility() {
        return ability;
    }

    public void setAbility(ComputerAbility ability) {
        this.ability = ability;
    }

    public ComputerFeels getFeels() {
        return feels;
    }

    public void setFeels(ComputerFeels feel) {
        this.feels = feel;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public Pawn.pawnColors getPawnColor() {
        return pawnColor;
    }

    public void setPawnColor(Pawn.pawnColors pawnColor) {
        this.pawnColor = pawnColor;
    }

}



