package main.models;

import main.game.Board;
import main.game.Pawn;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;
import main.game.Pawn.pawnColors;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class GameModel {

    private int gameId;
    private GameStatus gameStatus;
    private String gameName;
    private DeckModel gameDeck;
    private int numOpponents;

    private ArrayList<PlayerModel> players = new ArrayList<>();

    public enum GameStatus {
        setting_up, in_progress, complete
    }

    //Method to choose the order that the colors will take their turns
    public static ArrayList<pawnColors> whoGoesFirst() {
        ArrayList<pawnColors> orderColors = new ArrayList<>();
        orderColors.add(pawnColors.Red);
        orderColors.add(pawnColors.Blue);
        orderColors.add(pawnColors.Yellow);
        orderColors.add(pawnColors.Green);
        Collections.shuffle(orderColors);
        return orderColors;
    }

    public boolean createNewGame(String gameName) {
        SorryAPIResponse response = SorryAPI.createGame(gameName);
        if (response.isSuccess()) {
            int gameId = response.getData().get("id").getAsInt();
            setGameId(gameId);
            DeckModel deck = new DeckModel(this);
            setGameDeck(deck);
            getGameDeck().cardReshuffle();
            return saveDeck();
        } else {
            return false;
        }
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public DeckModel getGameDeck() {
        return gameDeck;
    }

    public void setGameDeck(DeckModel gameDeck) {
        this.gameDeck = gameDeck;
    }

    public int getNumOpponents() {
        return numOpponents;
    }

    public void setNumOpponents(int numOpponents) {
        this.numOpponents = numOpponents;
    }

    public ArrayList<PlayerModel> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<PlayerModel> players) {
        this.players = players;
    }

    public void addPlayer(PlayerModel player) {
        ArrayList<PlayerModel> newPlayers = getPlayers();
        newPlayers.add(player);
        setPlayers(getPlayers());
    }

    public boolean saveDeck() {
        SorryAPIResponse response = SorryAPI.saveGameDeck(gameId, gameDeck);
        return response.isSuccess();
    }

    public void initializePawnsInStart() {

    }

    public void addPawnsToBoard(Board board) {
        for (int i = 0; i < getPlayers().size(); i++) {
            PlayerModel player = getPlayers().get(i);
            for (int j = 0; j < 4; j++) {
                board.addPawnToBoard(new Pawn(player, board));
            }
        }
    }

}
