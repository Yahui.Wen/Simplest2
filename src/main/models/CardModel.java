package main.models;

public class CardModel extends ApplicationModel {

    private boolean moveFromStart = false;
    private boolean drawAgain = false;
    private boolean splitPawns = false;
    private boolean positionSwappable = false;
    private int cardNumber; // A 'Sorry Card' is 0
    private int forwardMoves = 1;
    private int reverseMoves = 0;

    public CardModel(int cardNumber) {
        this.cardNumber = cardNumber;
        switch (cardNumber) {
            case 0:
                moveFromStart = true;
                positionSwappable = true;
                forwardMoves = 0;
                break;
            case 1:
                moveFromStart = true;
                break;
            case 2:
                moveFromStart = true;
                forwardMoves = 2;
                drawAgain = true;
                break;
            case 3:
                forwardMoves = 3;
                break;
            case 4:
                forwardMoves = 0;
                reverseMoves = 4;
                break;
            case 5:
                forwardMoves = 5;
                break;
            case 7:
                positionSwappable = true;
                forwardMoves = 7;
                break;
            case 8:
                forwardMoves = 8;
                break;
            case 10:
                forwardMoves = 10;
                reverseMoves = 1;
                break;
            case 11:
                positionSwappable = true;
                forwardMoves = 11;
                break;
            case 12:
                forwardMoves = 12;
                break;
        }
    }

    public boolean isMoveFromStart() {
        return moveFromStart;
    }

    public void setMoveFromStart(boolean moveFromStart) {
        this.moveFromStart = moveFromStart;
    }

    public boolean isDrawAgain() {
        return drawAgain;
    }

    public void setDrawAgain(boolean drawAgain) {
        this.drawAgain = drawAgain;
    }

    public boolean isSplitPawns() {
        return splitPawns;
    }

    public void setSplitPawns(boolean splitPawns) {
        this.splitPawns = splitPawns;
    }

    public boolean isPositionSwappable() {
        return positionSwappable;
    }

    public void setPositionSwappable(boolean positionSwappable) {
        this.positionSwappable = positionSwappable;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getForwardMoves() {
        return forwardMoves;
    }

    public void setForwardMoves(int forwardMoves) {
        this.forwardMoves = forwardMoves;
    }

    public int getReverseMoves() {
        return reverseMoves;
    }

    public void setReverseMoves(int reverseMoves) {
        this.reverseMoves = reverseMoves;
    }

}
