package main;

import main.controllers.ApplicationController;
import main.controllers.MainMenuController;
import main.controllers.SplashController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import main.models.GameModel;

import java.io.IOException;

public class Sorry extends Application {

    public static Stage splashWindow;
    public static Stage applicationWindow;
    public static GameModel theGame = new GameModel();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent splash = FXMLLoader.load(getClass().getResource(new SplashController().getViewPath()));
        splashWindow = primaryStage;
        splashWindow.initStyle(StageStyle.UNDECORATED);
        splashWindow.setScene(new Scene(splash, SplashController.width, SplashController.height));
        splashWindow.show();
        // TODO: 4/3/18 Add a delay here.
        loadApplication();
    }

    public void loadApplication() throws IOException {
        splashWindow.close();
        applicationWindow = new Stage();
        applicationWindow.setTitle("Main Menu");
        Parent mainMenu = FXMLLoader.load(getClass().getResource(new MainMenuController().getViewPath()));
        mainMenu.getStylesheets().add("/main/resources/styles/global.css");
        applicationWindow.setScene(new Scene(mainMenu, ApplicationController.defaultWidth, ApplicationController.defaultHeight));
        applicationWindow.show();
    }

}