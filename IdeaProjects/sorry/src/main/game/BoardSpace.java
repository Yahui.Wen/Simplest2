package main.game;

import javafx.scene.layout.Pane;
import java.util.ArrayList;

import static main.game.BoardSpace.spaceType.*;
import static main.game.BoardSpace.spaceType.blueEndSlide;

public class BoardSpace extends Pane {

    //**(Note: Need to add additional values for the start/end of the slides)**
    public enum spaceType {
        blueStart, yellowStart, greenStart, redStart, blueHome, yellowHome, greenHome, redHome,
        blueSlide, yellowSlide, greenSlide, redSlide, blueSafety, yellowSafety, greenSafety, redSafety, normal,
        yellowStartSlide, yellowEndSlide, blueStartSlide, blueEndSlide, redStartSlide, redEndSlide, greenStartSlide, greenEndSlide
    }


    private int xPosition;
    private int yPosition;
    private BoardSpace.spaceType spaceType;
    private int priority;
    private int homeSpace;

    private ArrayList<Pawn> pawns = new ArrayList<>();

    public BoardSpace(int x, int y, BoardSpace.spaceType type, int priority) {
        super();
        setxPosition(x);
        setyPosition(y);
        setSpaceType(type);
        setPriority(priority);
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public BoardSpace.spaceType getSpaceType() {
        return spaceType;
    }

    public void setSpaceType(BoardSpace.spaceType spaceType) {
        this.spaceType = spaceType;
    }
    public int getHomeSpace(){
        return homeSpace;
    }

    public void setHomeSpace(int homeSpace) {
        this.homeSpace = homeSpace;
    }

    public ArrayList<Pawn> getPawns() {
        return pawns;
    }

    public void setPawns(ArrayList<Pawn> pawns) {
        this.pawns = pawns;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isStartSpace() {
        switch (getSpaceType()) {
            case blueStart:
            case yellowStart:
            case greenStart:
            case redStart:
                return true;
            default:
                return false;
        }
    }

    public boolean isHomeSpace() {
        switch (getSpaceType()) {
            case blueHome:
            case yellowHome:
            case greenHome:
            case redHome:
                return true;
            default:
                return false;
        }
    }

    public boolean isSlideSpace() {
        switch (getSpaceType()) {
            case blueSlide:
            case yellowSlide:
            case greenSlide:
            case redSlide:
                return true;
            default:
                return false;
        }
    }

    public boolean isSafetySpace() {
        switch (getSpaceType()) {
            case blueSafety:
            case yellowSafety:
            case greenSafety:
            case redSafety:
                return true;
            default:
                return false;
        }
    }

    public boolean isSlideStart() {
        switch (getSpaceType()) {
            case blueStartSlide:
            case yellowStartSlide:
            case greenStartSlide:
            case redStartSlide:
                return true;
            default:
                return false;
        }
    }

    public boolean isSlideEnd() {
        switch (getSpaceType()) {
            case blueEndSlide:
            case yellowEndSlide:
            case greenEndSlide:
            case redEndSlide:
                return true;
            default:
                return false;
        }
    }


//    public static int slide(BoardSpace fromSpace) {
//        System.out.println("IN SLIDE");
//        int extraMoves = 0;
//        if (fromSpace.getSpaceType() == yellowStartSlide || fromSpace.getSpaceType() == redStartSlide || fromSpace.getSpaceType() == greenStartSlide
//                || fromSpace.getSpaceType() == blueStartSlide) {
//            boolean foundEndSlide = false;
//            BoardSpace currentBoardSpace = fromSpace;
//            while (!foundEndSlide) {
//                extraMoves++;
//                currentBoardSpace = nextPlaceFrom(currentBoardSpace);
//                if (currentBoardSpace.getSpaceType() == yellowEndSlide || currentBoardSpace.getSpaceType() == redEndSlide ||
//                        currentBoardSpace.getSpaceType() == greenEndSlide || currentBoardSpace.getSpaceType() == blueEndSlide) {
//                    foundEndSlide = true;
//                }
//            }
//        }
//        return extraMoves;

}
