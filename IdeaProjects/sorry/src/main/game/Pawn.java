package main.game;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;
import main.models.PlayerModel;

import java.util.ArrayList;

import static main.game.BoardSpace.spaceType.*;
import static main.game.Pawn.pawnColors.*;

/**
 * Naturally, a Pawn extends a Circle because... pawns are circles. This class also
 * aligns with pawns in the database, and will hold/update its positions, player, etc.
 * Pawns are also part of a Board, and a BoardSpace. This is so that the pawn can know
 * where it is in relation to other boards spaces.
 *
 * @author Andrew Hollar, Ryan Berliner, Sam Nguon, Yahui Wen
 */
public class Pawn extends Circle {
    /**
     * Pretty self explanatory... 'Empty' is used by the Sorry! API if for some reason the user
     * is able to try and create more than 4 players, the pawn color will be 'Empty' signifying
     * that there are no colors left to choose from and they cannot do what they are trying to do.
     */
    public enum pawnColors {
        Blue, Yellow, Green, Red, Empty
    }

    /**
     * Database entry fields.
     */
    private int pawnId;
    private int gamePlayerId;

    /**
     * Based on the pawn color, we will translate this to fill and stroke color. We will also have a highlight
     * stroke color for when we want to highlight a pawn.
     */
    private pawnColors color;
    private String fillColor;
    private String strokeColor;
    private String highlightStrokeColor;

    /**
     * These are only to be changed when the pawn is saved useing the savePawn() method. When this
     * method is called, it will updated these valued based on the x, y position of the board space
     * it is in. This is so we can save the pawn positions when someone is DONE with there turn and
     * we done run into trouble with people resuming a game halfway through there turn.
     */
    private int x;
    private int y;

    /**
     * These are more to keep track of the objects that this pawn is attatched to. It will allow us to
     * implement a move methods, and know which board we need to look at, and what board space we need
     * to remove the pawn from.
     */
    private Board board;
    private BoardSpace boardSpace;


    /**
     * The X and Y positions (on a 16x16 grid... 1-16) of the starts will never change. We keep these in a static
     * context because they don't depend on anything, but will act as a lookup table when
     * creating new pawns of different colors.
     */
    private static final int redStartX = 2;
    private static final int redStartY = 12;
    private static final int blueStartX = 5;
    private static final int blueStartY = 2;
    private static final int greenStartX = 12;
    private static final int greenStartY = 15;
    private static final int yellowStartX = 15;
    private static final int yellowStartY = 5;

    /**
     * When create a new game, all the pawns will need to be created (not fetched form the database). This
     * means that all pawns will be in there start positions, and we only know of the game player that we
     * need to attach them to. Once we create the pawn, we can assign it it's id. From the player we can
     * get all the information we need to create the pawn correctly.
     *
     * @param player The player the pawn belongs to.
     */
    public Pawn(PlayerModel player, Board board) {
        this.color = player.getPawnColor();
        this.gamePlayerId = player.getGamePlayerId();
        setStrokeWidth(3);
        switch (getColor()) {
            case Red:
                setX(redStartX);
                setY(redStartY);
                setFillColor("#A00003");
                setStrokeColor("#500A10");
                break;
            case Blue:
                setX(blueStartX);
                setY(blueStartY);
                setFillColor("#2D5FBE");
                setStrokeColor("#1A2361");
                break;
            case Green:
                setX(greenStartX);
                setY(greenStartY);
                setFillColor("#45831E");
                setStrokeColor("#203C1E");
                break;
            case Yellow:
                setX(yellowStartX);
                setY(yellowStartY);
                setFillColor("#C4C008");
                setStrokeColor("#C47306");
                break;
        }
        setBoard(board);
        setFill(Color.valueOf(getFillColor()));
        setStroke(Color.valueOf(getStrokeColor()));
        setHighlightStrokeColor("#000000"); // All highlight with black.
        // Create pawn entry in database, set the ID afterwards accordingly.
        SorryAPIResponse response = SorryAPI.createPawn(player.getGamePlayerId(), getX(), getY());
        this.pawnId = (response.isSuccess()) ? response.getData().get("pawn_id").getAsInt() : -1;
    }

    /**
     * Checks if the pawn is able to be placed in a given board space.
     *
     * @param space The board space you want to check for placement eligibility.
     *
     * @return Whether or not the pawn can be placed in the given space.
     */
    private boolean validPawnPlacement(BoardSpace space) {
        switch (space.getSpaceType()) {
            case blueStart:
            case blueHome:
            case blueSafety:
                return getColor() == Blue;
            case greenStart:
            case greenHome:
            case greenSafety:
                return getColor() == Green;
            case redStart:
            case redHome:
            case redSafety:
                return getColor() == Red;
            case yellowStart:
            case yellowHome:
            case yellowSafety:
                return getColor() == Yellow;
            default:
                // Normal spaces and sliders.
                return true;
        }
    }

    /**
     * Gives you the next place from a given board space.
     *
     * @param fromSpace The space you would like to simulate being on, and know what comes after.
     *
     * @return The boards space that comes after the fromSpace.
     * @see BoardSpace
     */
    public BoardSpace nextPlaceFrom(BoardSpace fromSpace) {
        int currentSpacePriority = fromSpace.getPriority();
        BoardSpace currentCandidateSpace = getBoard().getBoardSpaces().get(0);
        int currentCandidateDifference = 999999; // Arbitrarily large difference.
        for (int i = 0; i < getBoard().getBoardSpaces().size(); i++) {
            BoardSpace space = getBoard().getBoardSpaces().get(i);
            if (space.getPriority() == 0 || !validPawnPlacement(space)) {
                continue;
            }
            int difference = Math.abs(space.getPriority() - currentSpacePriority);
            if (space.getPriority() > currentSpacePriority && difference < currentCandidateDifference) {
                currentCandidateDifference = difference;
                currentCandidateSpace = space;
            }
        }
        return currentCandidateSpace;
    }

    /**
     * Gives you the previous place from a given board space.
     *
     * @param fromSpace The space you would like to simulate being on, and know what comes after.
     *
     * @return The boards space that comes before the fromSpace.
     * @see BoardSpace
     */
    public BoardSpace previousPlaceFrom(BoardSpace fromSpace) {
        int currentSpacePriority = fromSpace.getPriority();
        BoardSpace currentCandidateSpace = getBoard().getBoardSpaces().get(0);
        int currentCandidateDifference = 999999; // Arbitrarily large difference.
        for (int i = 0; i < getBoard().getBoardSpaces().size(); i++) {
            BoardSpace space = getBoard().getBoardSpaces().get(i);
            if (space.getPriority() == 0 || !validPawnPlacement(space)) {
                continue;
            }
            // TODO: 4/23/18 This check shouldn't have hard coded priorities. 
            if (space.getPriority() == 600 && fromSpace.getPriority() == 10) {
                return space;
            }
            int difference = Math.abs(space.getPriority() - currentSpacePriority);
            if (space.getPriority() < currentSpacePriority && difference < currentCandidateDifference && !space.isStartSpace()) {
                if ((fromSpace.getSpaceType() != normal && !fromSpace.isSlideSpace()) || (!space.isSafetySpace() && !space.isHomeSpace())) {
                    currentCandidateDifference = difference;
                    currentCandidateSpace = space;
                }
            }
        }
        return (canGoBackwards()) ? currentCandidateSpace : fromSpace;
    }

    /**
     * The next board space for the pawn to go to.
     *
     * @return The next board space.
     * @see BoardSpace
     */
    public BoardSpace nextPlace() {
        return nextPlaceFrom(getBoardSpace());
    }

    /**
     * The previous board space for the pawn to go.
     *
     * @return The previous board space.
     *
     * @see BoardSpace
     */
    private BoardSpace previousPlace() {
        return previousPlaceFrom(getBoardSpace());
    }

    /**
     * Moves the pawn to the next space (if it can)
     */
    public void gotoNextPlace() {
        setBoardSpace(nextPlace());
    }

    /**
     * Moves the pawn to the previous space (if it can).
     */
    public void gotoPreviousPlace() {
        setBoardSpace(previousPlace());
    }

    /**
     * Get the pawn id, this is the database id.
     *
     * @return Database id of the pawn.
     */
    public int getPawnId() {
        return pawnId;
    }

    /**
     * Gets the game player id of the pawn. (database id)
     *
     * @return Database of the game player id.
     */
    public int getGamePlayerId() {
        return gamePlayerId;
    }

    /**
     * Get the color of the pawn.
     *
     * @return The pawn color.
     * @see pawnColors
     */
    public pawnColors getColor() {
        return color;
    }

    /**
     * Get the x position of the pawn. This will only match the current x position of the pawn
     * after the savePawnPosition() method has been called.
     *
     * @return The last saved x position of the pawn.
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x position of the pawn.
     *
     * @param x The x position.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Get the y position of the pawn. This will only match the current y position of the pawn
     * after the savePawnPosition() method has been called.
     *
     * @return The last saved y position of the pawn.
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y position of the pawn.
     *
      * @param y The y position.
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Gets the board the pawn is on.
     *
     * @return The board.
     * @see Board
     */
    public Board getBoard() {
        return board;
    }

    /**
     * Sets the board the pawn is apart of.
     *
     * @param board The board to associate the pawn with.
     */
    public void setBoard(Board board) {
        this.board = board;
    }

    /**
     * Gets the board space the pawn is currently associated with.
     *
     * @return The board space the pawn is associated with.
     * @see BoardSpace
     */
    public BoardSpace getBoardSpace() {
        return boardSpace;
    }

    /**
     * Sets the board space that the pawn is apart of. When the board space of the pawn is set,
     * we add it to the boards spaces pawns as well. In addition, we add it to the children of
     * the board space so that it will update visually on the board.
     *
     * There is also some positioning happening to make sure they are centered in the spaces.
     *
     * <b>Note:</b> Before doing anything, we check if there is a currently associated board
     * space with the pawn. If there is, we need to remove it from the child of that space so
     * we can add it to the children of the new space. We also remove it from the array list
     * of pawns for the old space to.
     *
     * @param boardSpace The board space to move the pawn to.
     */
    public void setBoardSpace(BoardSpace boardSpace) {
        if (getBoardSpace() != null) {
            getBoardSpace().getPawns().remove(this);
            getBoardSpace().getChildren().remove(this);
        }
        this.boardSpace = boardSpace;
        switch (getBoardSpace().getSpaceType()) {
            // TODO: 4/20/18 Really need to figure this out... spacing out pawns on start and home.
            default:
                layoutXProperty().bind(getBoardSpace().widthProperty().divide(2));
                layoutYProperty().bind(getBoardSpace().heightProperty().divide(2));
                break;
        }
        ArrayList<Pawn> pawns = getBoardSpace().getPawns();
        pawns.add(this);
        getBoardSpace().setPawns(pawns);
        getBoardSpace().getChildren().add(this);
    }

    /**
     * Save the position of the pawn. It doesn't this by getting the x, y position of the board space
     * it is associated with, and the updating it's own x and y position with that info.
     *
     * Then it just makes a call to the Sorry! API and updates the values.
     *
     * @return If it updated in the database successfully or not.
     */
    public boolean savePawnPosition() {
        setX(getBoardSpace().getxPosition());
        setY(getBoardSpace().getyPosition());
        SorryAPIResponse response = SorryAPI.updatePawnPosition(getPawnId(), getX(), getY());
        return response.isSuccess();
    }

    /**
     * Checks if a pawn has the ability to move backwards. This is true for every space except for
     * the start positions.
     *
     * @return Whether or not a pawn can move backwards.
     */
    public boolean canGoBackwards() {
        switch (getBoardSpace().getSpaceType()) {
            case blueStart:
            case redStart:
            case yellowStart:
            case greenStart:
                return false;
            default:
                return true;
        }
    }

    /**
     * Checks if a pawn has the ability to move forwards. This is true for every space except for
     * the finish positions.
     *
     * @return Whether or not a pawn can move forwards.
     */
    public boolean canGoForwards() {
        return canGoForwardsFrom(getBoardSpace());
    }

    /**
     * Checks if a pawn has the ability to move forwards. This is true for every space except for
     * the finish positions.
     *
     * @return Whether or not a pawn can move forwards.
     */
    public boolean canGoForwardsFrom(BoardSpace boardSpace) {
        switch (boardSpace.getSpaceType()) {
            case blueHome:
            case redHome:
            case yellowHome:
            case greenHome:
                return false;
            default:
                return true;
        }
    }

    /**
     * Get the fill color (hexidecimal)
     *
     * @return The hexidecimal fill color.
     */
    public String getFillColor() {
        return fillColor;
    }

    /**
     * Set the fill color (hexidecimal)
     *
     * @param fillColor The color to fill the pawn (hexidecimal)
     */
    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * Get the stroke color (hexidecimal)
     *
     * @return The hexidecimal stroke color.
     */
    public String getStrokeColor() {
        return strokeColor;
    }

    /**
     * Set the stroke color (hexidecimal)
     *
     * @param strokeColor The color for the stroke of the pawn (hexidecimal)
     */
    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    /**
     * Get the color of the stoke when the pawn is highlighted (hexidecimal)
     *
     * @return The color of the stroke when the pawn is highlighted (hexidecimal)
     */
    public String getHighlightStrokeColor() {
        return highlightStrokeColor;
    }

    /**
     * Set the color of the stroke when the pawn is highlighted (hexidecimal).
     *
     * @param highlightStrokeColor The color of the stroke when the pawn is highlighted (hexidecimal)
     */
    public void setHighlightStrokeColor(String highlightStrokeColor) {
        this.highlightStrokeColor = highlightStrokeColor;
    }

    /**
     * This will set the stroke color to the highlight stroke color.
     */
    public void highlightPawn() {
        setStroke(Color.valueOf(getHighlightStrokeColor()));
    }

    /**
     * This will set the stroke color to the normal stroke color.
     */
    public void unHighlightPawn() {
        setStroke(Color.valueOf(getStrokeColor()));
    }

    /**
     * Moves a pawn back to its home space.
     */
    public void goHome() {
        int homeSpaceIndex = 0;
        for (int i = 0; i < getBoard().getBoardSpaces().size(); i++) {
            BoardSpace space = getBoard().getBoardSpaces().get(i);
            if (space.getSpaceType() == blueStart && getColor() == Blue) {
                homeSpaceIndex = i;
            } else if (space.getSpaceType() == redStart && getColor() == Red) {
                homeSpaceIndex = i;
            } else if (space.getSpaceType() == yellowStart && getColor() == Yellow) {
                homeSpaceIndex = i;
            } else if (space.getSpaceType() == greenStart && getColor() == Green) {
                homeSpaceIndex = i;
            }
        }
        BoardSpace home = getBoard().getBoardSpaces().get(homeSpaceIndex);
        setBoardSpace(home);
    }

    public boolean canMoveNSpaces(int n) {
        BoardSpace nextPlace = getBoardSpace();
        for (int i = 0; i < n; i++) {
            BoardSpace temp = nextPlace;
            nextPlace = nextPlaceFrom(nextPlace);
            if (temp == nextPlace || !canGoForwardsFrom(temp)) {
                return false;
            }
        }
        if (getNthSpace(n).getPawns().size() > 0 && getNthSpace(n).getPawns().get(0).getColor() == getColor()) {
            return false;
        }
        return true;
    }

    public boolean canMoveNSpacesBack(int n) {
        BoardSpace previousPlace = getBoardSpace();
        for (int i = 0; i < n; i++) {
            BoardSpace temp = previousPlace;
            previousPlace = previousPlaceFrom(previousPlace);
            if (temp == previousPlace) {
                return false;
            }
        }
        if (getNthSpaceBack(n).getPawns().size() > 0 && getNthSpaceBack(n).getPawns().get(0).getColor() == getColor()) {
            return false;
        }
        return true;
    }

    public BoardSpace getNthSpace(int n) {
        BoardSpace lastSpace = getBoardSpace();
        for (int i = 0; i < n; i++) {
            lastSpace = nextPlaceFrom(lastSpace);
        }
        return lastSpace;
    }

    public BoardSpace getNthSpaceBack(int n) {
        BoardSpace lastSpace = getBoardSpace();
        for (int i = 0; i < n; i++) {
            lastSpace = previousPlaceFrom(lastSpace);
        }
        return lastSpace;
    }

    public boolean pawnsInNSpaces(int n) {
        BoardSpace nthSpace = getNthSpace(n);
        return nthSpace.getPawns().size() != 0;
    }

    public boolean pawnsInNSpacesBack(int n) {
        BoardSpace nthSpace = getNthSpaceBack(n);
        return nthSpace.getPawns().size() != 0;
    }

    public int movesToHome() {
        BoardSpace nextPlace = nextPlace();
        int totalRemaingingMoves = 0;
        while (!nextPlace.isHomeSpace()) {
            totalRemaingingMoves++;
            nextPlace = nextPlaceFrom(nextPlace);
        }
        return totalRemaingingMoves + 1;
    }

    /**
     * This will check if the pawn is on the beggining of a slider that it can slide. If it is, it will
     * return the number of spaces that it may move forward.
     *
     * @return The number of extra spaces it will move forward.
     */
//    public int intSliderEffect() {
//
//        BoardSpace previousSpace = previousPlace();
//    }
}
