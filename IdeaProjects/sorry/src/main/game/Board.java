package main.game;

import java.lang.reflect.Array;
import java.util.ArrayList;

import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import main.Sorry;
import main.game.BoardSpace.spaceType;
import main.helpers.ComputerTurn;
import main.models.CardModel;
import main.models.GameModel;

import static main.game.BoardSpace.spaceType.*;

public class Board extends Pane {

    public static int boardWidth = 16;
    public static int boardHeight = 16;

    private ArrayList<BoardSpace> boardSpaces;

    public Board() {
        boardSpaces = new ArrayList<BoardSpace>();
        for (int h = 1; h <= boardHeight; h++) {
            for (int w = 1; w <= boardWidth; w++) {
                boardSpaces.add(new BoardSpace(w, h, spaceType.normal, 0));
            }
        }
        initializeSpaceIds();
        setPriorities();
    }

    public ArrayList<BoardSpace> getBoardSpaces() {
        return boardSpaces;
    }

    public void setBoardSpaces(ArrayList<BoardSpace> boardSpaces) {
        this.boardSpaces = boardSpaces;
    }

    public void setPriorities() {
        setTopPriorities();
        setRightPriorities();
        setBottomPriorities();
        setLeftPriorities();
    }

    public void setTopPriorities() {
        int priority = 10;
        for (int i = 0; i < 16; i++) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    public void setRightPriorities() {
        int priority = 170;
        for (int i = 31; i < 256; i += 16) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    public void setBottomPriorities() {
        int priority = 320;
        for (int i = 254; i >= 240; i--) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    public void setLeftPriorities() {
        int priority = 470;
        for (int i = 224; i >= 16; i -= 16) {
            boardSpaces.get(i).setPriority(priority);
            priority += 10;
        }
    }

    //Sets the spaceType for start spaces, home spaces, slide spaces.
    //**(Note: This does not differentiate between the start of slides and end of slides)**
    public void initializeSpaceIds() {
        initializeHomeAndSafetySpaceIds();
        initializeStartSpaceIds();
        initializeSliderSpaceIds();
    }

    private void initializeHomeAndSafetySpaceIds() {
        //initialize Blue home spaces
        int[] blueHome = {18, 34, 50, 66, 82, 98};
        //int blueSafetyIndex = 98;
        int blueHomePriority = 31;
        for (int homeSpaceIndex : blueHome) {
            if (homeSpaceIndex == 98) {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.blueHome);
            } else {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.blueSafety);
            }
            boardSpaces.get(homeSpaceIndex).setPriority(blueHomePriority);
            blueHomePriority++;
        }

        //initialize Yellow home spaces
        int yellowHomeSpaceStart = 46;
        int yellowHomeSpaceEnd = 41;
        int yellowHomePriority = 181;
        for (int i = yellowHomeSpaceStart; i >= yellowHomeSpaceEnd; i--) {
            if (i == yellowHomeSpaceEnd) {
                boardSpaces.get(i).setSpaceType(spaceType.yellowHome);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.yellowSafety);
            }
            boardSpaces.get(i).setPriority(yellowHomePriority);
            yellowHomePriority++;
        }

        //initialize Green home spaces
        int[] greenHome = {237, 221, 205, 189, 173, 157};
        int greenHomePriority = 331;
        for (int homeSpaceIndex : greenHome) {
            if (homeSpaceIndex == 157) {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.greenHome);
            } else {
                boardSpaces.get(homeSpaceIndex).setSpaceType(spaceType.greenSafety);
            }
            boardSpaces.get(homeSpaceIndex).setPriority(greenHomePriority);
            greenHomePriority++;
        }

        //initialize Red home spaces
        int redHomeSpaceStart = 209;
        int redHomeSpaceEnd = 214;
        int redHomePriority = 481;
        for (int i = redHomeSpaceStart; i <= redHomeSpaceEnd; i++) {
            if (i == redHomeSpaceEnd) {
                boardSpaces.get(i).setSpaceType(redHome);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.redSafety);
            }
            boardSpaces.get(i).setPriority(redHomePriority);
            redHomePriority++;
        }
    }

    private void initializeStartSpaceIds() {
        //initialize Red start space
        int[] redStartIndex = {177, 178, 162, 194};
        for (int index : redStartIndex) {
            boardSpaces.get(index).setSpaceType(spaceType.redStart);
            boardSpaces.get(index).setPriority(499);
        }

        //initialize Green start space
        int[] greenStartIndex = {235, 219, 220, 218};
        for (int index : greenStartIndex) {
            boardSpaces.get(index).setSpaceType(spaceType.greenStart);
            boardSpaces.get(index).setPriority(349);
        }

        //initialize Yellow start space
        int[] yellowStartIndex = {78, 77, 93, 61};
        for (int index : yellowStartIndex) {
            boardSpaces.get(index).setSpaceType(spaceType.yellowStart);
            boardSpaces.get(index).setPriority(199);
        }
        //initialize Blue start space
        int[] blueStartIndex = {20, 35, 36, 37};
        for (int index : blueStartIndex) {
            boardSpaces.get(index).setSpaceType(spaceType.blueStart);
            if (index==20){
                boardSpaces.get(index).setHomeSpace(20);
            }else if (index==35){
                boardSpaces.get(index).setHomeSpace(35);
            }else if (index==36){
                boardSpaces.get(index).setHomeSpace(36);
            }else if (index==37){
                boardSpaces.get(index).setHomeSpace(37);
            }
            boardSpaces.get(index).setPriority(49);
        }
    }


    private void initializeSliderSpaceIds() {
        //initialize Red slide spaces
        int[] redSlide1 = {224, 208, 192, 176};
        for (int slideSpaceIndex : redSlide1) {
            if (slideSpaceIndex == 224) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redStartSlide);
            } else if (slideSpaceIndex == 176) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redEndSlide);
            } else {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redSlide);
            }
        }
        int[] redSlide2 = {96, 80, 64, 48, 32};
        for (int slideSpaceIndex : redSlide2) {
            if (slideSpaceIndex == 96) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redStartSlide);
            } else if (slideSpaceIndex == 32) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redEndSlide);
            } else {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.redSlide);
            }
        }

        //initialize Green slide spaces
        int greenSlide1StartIndex = 254;
        int greenSlide1EndIndex = 251;
        for (int i = greenSlide1StartIndex; i >= greenSlide1EndIndex; i--) {
            if (i == 254) {
                boardSpaces.get(i).setSpaceType(spaceType.greenStartSlide);
            } else if (i == 251) {
                boardSpaces.get(i).setSpaceType(spaceType.greenEndSlide);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.greenSlide);
            }
        }
        int greenSlide2StartIndex = 246;
        int greenSlide2EndIndex = 242;
        for (int i = greenSlide2StartIndex; i >= greenSlide2EndIndex; i--) {
            if (i == 246) {
                boardSpaces.get(i).setSpaceType(spaceType.greenStartSlide);
            } else if (i == 242) {
                boardSpaces.get(i).setSpaceType(spaceType.greenEndSlide);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.greenSlide);
            }
        }

        //initialize Yellow slide spaces
        int[] yellowSlide1 = {31, 47, 63, 79};
        for (int slideSpaceIndex : yellowSlide1) {
            if (slideSpaceIndex == 31) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowStartSlide);
            } else if (slideSpaceIndex == 79) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowEndSlide);
            } else {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowSlide);
            }
        }
        int[] yellowSlide2 = {159, 175, 191, 207, 223};
        for (int slideSpaceIndex : yellowSlide2) {
            if (slideSpaceIndex == 159) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowStartSlide);
            } else if (slideSpaceIndex == 223) {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowEndSlide);
            } else {
                boardSpaces.get(slideSpaceIndex).setSpaceType(spaceType.yellowSlide);
            }
        }

        //initialize Blue slide spaces
        int blueSlide1StartIndex = 1;
        int blueSlide1EndIndex = 4;
        for (int i = blueSlide1StartIndex; i <= blueSlide1EndIndex; i++) {
            if (i == 1) {
                boardSpaces.get(i).setSpaceType(spaceType.blueStartSlide);
            } else if (i == 4) {
                boardSpaces.get(i).setSpaceType(spaceType.blueEndSlide);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.blueSlide);
            }
        }
        int blueSlide2StartIndex = 9;
        int blueSlide2EndIndex = 13;
        for (int i = blueSlide2StartIndex; i <= blueSlide2EndIndex; i++) {
            if (i == 9) {
                boardSpaces.get(i).setSpaceType(spaceType.blueStartSlide);
            } else if (i == 13) {
                boardSpaces.get(i).setSpaceType(spaceType.blueEndSlide);
            } else {
                boardSpaces.get(i).setSpaceType(spaceType.blueSlide);
            }
        }
    }

    public static Pane getPaneFromGridPane(GridPane gridPane, int col, int row) {
        for (Node pane : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(pane) == col && GridPane.getRowIndex(pane) == row) {
                return (Pane) pane;
            }
        }
        return null;
    }
    
    public void addBoardSpacesToGrid(GridPane grid) {
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            BoardSpace space = getBoardSpaces().get(i);
            switch (space.getSpaceType()) {
                case blueSlide:
                case blueStartSlide:
                case blueEndSlide:
                    space.setStyle("-fx-background-color: rgba(0,0,255,.5);");
                    break;
                case redSlide:
                case redStartSlide:
                case redEndSlide:
                    space.setStyle("-fx-background-color: rgba(255,0,0,.5);");
                    break;
                case yellowSlide:
                case yellowStartSlide:
                case yellowEndSlide:
                    space.setStyle("-fx-background-color: rgba(255,255,0,.5);");
                    break;
                case greenSlide:
                case greenStartSlide:
                case greenEndSlide:
                    space.setStyle("-fx-background-color: rgba(0,255,0,.5);");
                    break;
            }
            grid.add(space, i % 16, i / 16);
        }
    }

    public void resize(GridPane grid) {
        double gridHeight = grid.getHeight();
        double cellHeight = gridHeight / 16;
        doResizing(cellHeight);
    }

    public void doResizing(double cellHeight) {
        double defaultSpaceDimensions = cellHeight;
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            double newSpaceDimension = defaultSpaceDimensions;
            BoardSpace space = getBoardSpaces().get(i);
            switch (space.getSpaceType()) {
                case blueStart:
                case blueHome:
                case redStart:
                case redHome:
                case yellowStart:
                case yellowHome:
                case greenStart:
                case greenHome:
                    // TODO: 4/18/18 make the spaces bigger, and figure out how to position them.
//                     newSpaceDimension *= 2;
                    break;
            }
            switch (space.getSpaceType()) {
                case blueStart:
                case blueHome:
                    break;
            }
            space.setMaxHeight(newSpaceDimension);
            space.setMaxWidth(newSpaceDimension);
            space.setMinHeight(newSpaceDimension);
            space.setMinWidth(newSpaceDimension);
        }
        sizePawns(defaultSpaceDimensions);
    }

    public void sizePawns(double spaceDimension) {
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            for (int j = 0; j < getBoardSpaces().get(i).getPawns().size(); j++) {
                Pawn pawn = getBoardSpaces().get(i).getPawns().get(j);
                pawn.setRadius(spaceDimension / 4);
            }
        }
    }


    public void draw(GridPane grid) {
        addBoardSpacesToGrid(grid);
    }

    public void addPawnToBoard(Pawn pawn,Pawn pawn2,Pawn pawn3,Pawn pawn4) {
        int pawnX2 = pawn2.getX2();
        int pawnY2 = pawn2.getY2();
        int pawnX1 = pawn.getX1();
        int pawnY1 = pawn.getY1();
        int pawnX3 = pawn3.getX3();
        int pawnY3 = pawn3.getY3();
        int pawnX4 = pawn4.getX4();
        int pawnY4 = pawn4.getY4();
        for (int i = 0; i < getBoardSpaces().size(); i++) {
            BoardSpace space = getBoardSpaces().get(i);
            if ((space.getxPosition() == pawnX1 && space.getyPosition() == pawnY1)){
                pawn.setBoardSpace(space);
            }
            if ((space.getxPosition() == pawnX2 && space.getyPosition() == pawnY2)){
                pawn2.setBoardSpace(space);
            }
            if ((space.getxPosition() == pawnX3 && space.getyPosition() == pawnY3)){
                pawn3.setBoardSpace(space);
            }
            if ((space.getxPosition() == pawnX4 && space.getyPosition() == pawnY4)){
                pawn4.setBoardSpace(space);
            }

        }
    }


    public static ArrayList<ComputerTurn> calculateAllMoves(GameModel game) {
        ArrayList<ComputerTurn> allMoves = new ArrayList<>();
        CardModel card = game.getGameDeck().getCurrentCard();
        Pawn.pawnColors currentPlayerColor = game.getCurrentPlayer().getPawnColor();
        ArrayList<Pawn> currentPlayerPawns = getPawnsOfAColor(game.getBoard(), currentPlayerColor);
        // Fresh snapshot before any move.
        ComputerTurn turn;
        for (int i = 0; i < currentPlayerPawns.size(); i++) {

            // Get the pawn so we can try things with it.
            Pawn pawn = currentPlayerPawns.get(i);

            // Fresh snapshot.
            turn = new ComputerTurn(game);

            // Can they move from start?
            if (card.isMoveFromStart() && pawn.getBoardSpace().isStartSpace() && pawn.canGoForwards()) {
                if (pawn.nextPlace().getPawns().size() == 0 || pawn.nextPlace().getPawns().get(0).getColor() != pawn.getColor()) {
                    turn.setPawnId(pawn.getPawnId());
                    turn.setDidMoveFromStart(true);
                    turn.setMoves(1);
                    // We move out pawns 1 closer to the finish.
                    turn.setEndMovesToCompletion(turn.getStartMovesToCompletion() - 1);
                    // If there was a pawn in the other space, lets get its distance from its home, so we can subtract that
                    // from the opponent start to completion.
                    if (pawn.nextPlace().getPawns().size() > 0) {
                        Pawn affectedPawn = pawn.nextPlace().getPawns().get(0);
                        int startMovesToCompletion = affectedPawn.movesToHome();
                        turn.addAffectedPawnId(affectedPawn.getPawnId());
                        turn.setEndOpponentMovesToCompletion(turn.getEndOpponentMovesToCompletion() + startMovesToCompletion);
                    }
                    System.out.println("can move from start");
                    allMoves.add(turn);

                }
            }

            // Fresh snapshot.
            turn = new ComputerTurn(game);

            // Can they move it forward?
            if (card.getForwardMoves() > 0 && pawn.canMoveNSpaces(card.getForwardMoves()) && !pawn.getBoardSpace().isStartSpace()) {
                boolean stillValid = true;

                turn.setPawnId(pawn.getPawnId());
                turn.setDidMoveFromStart(false);
                turn.setMoves(card.getForwardMoves());

                if (pawn.getNthSpace(card.getForwardMoves()).isSlideStart()) {
                    BoardSpace temp = pawn.getNthSpace(card.getForwardMoves());
                    while (!temp.isSlideEnd()) {
                        temp = pawn.nextPlaceFrom(temp);
                        if (temp.getPawns().size() != 0) {
                            if (temp.getPawns().get(0).getColor() != pawn.getColor()) {
                                turn.addAffectedPawnId(temp.getPawns().get(0).getPawnId());
                            } else {
                                stillValid = false;
                            }
                        }
                        turn.setMoves(turn.getMoves() + 1);
                    }
                }

                // Move out pawns however many forwardMoves they have available to them
                turn.setEndMovesToCompletion(turn.getStartMovesToCompletion() - card.getForwardMoves());

                if (pawn.pawnsInNSpaces(card.getForwardMoves())) {
                    Pawn affectedPawn = pawn.getNthSpace(card.getForwardMoves()).getPawns().get(0);
                    int startMovesToCompletion = affectedPawn.movesToHome();
                    turn.addAffectedPawnId(affectedPawn.getPawnId());
                    turn.setEndOpponentMovesToCompletion(turn.getEndOpponentMovesToCompletion() + startMovesToCompletion);
                }
                if (stillValid) {
                    allMoves.add(turn);
                }
            }

            // Fresh snapshot.
            turn = new ComputerTurn(game);

            // Can they move it backwards?
            if (card.getReverseMoves() > 0 && pawn.canMoveNSpacesBack(card.getReverseMoves())) {
                boolean stillValid = true;

                turn.setPawnId(pawn.getPawnId());
                turn.setDidMoveFromStart(false);
                turn.setMoves(-1 * card.getReverseMoves());

                if (pawn.getNthSpaceBack(card.getReverseMoves()).isSlideStart()) {
                    BoardSpace temp = pawn.getNthSpaceBack(card.getReverseMoves());
                    while (!temp.isSlideEnd()) {
                        temp = pawn.nextPlaceFrom(temp);
                        turn.setMoves(turn.getMoves() + 1);
                        if (temp.getPawns().size() != 0) {
                            if (temp.getPawns().get(0).getColor() != pawn.getColor()) {
                                turn.addAffectedPawnId(temp.getPawns().get(0).getPawnId());
                            } else {
                                stillValid = false;
                            }
                        }
                    }
                }

                // Move out pawns however many reverse moves they have available to them
                turn.setEndMovesToCompletion(turn.getStartMovesToCompletion() + card.getReverseMoves());

                if (pawn.pawnsInNSpacesBack(card.getReverseMoves())) {
                    Pawn affectedPawn = pawn.getNthSpaceBack(card.getReverseMoves()).getPawns().get(0);
                    int startMovesToCompletion = affectedPawn.movesToHome();
                    turn.addAffectedPawnId(affectedPawn.getPawnId());
                    turn.setEndOpponentMovesToCompletion(turn.getEndOpponentMovesToCompletion() + startMovesToCompletion);
                }
                if (stillValid) {
                    allMoves.add(turn);
                }
            }
        }
        return allMoves;
    }

    public static ArrayList<Pawn> getPawnsOfAColor(Board board, Pawn.pawnColors color) {
        ArrayList<Pawn> pawnsOfAColor = new ArrayList<>();
        for (int i = 0; i < board.getBoardSpaces().size(); i++) {
            for (int j = 0; j < board.getBoardSpaces().get(i).getPawns().size(); j++) {
                if (board.getBoardSpaces().get(i).getPawns().get(j).getColor() == color) {
                    pawnsOfAColor.add(board.getBoardSpaces().get(i).getPawns().get(j));
                }
            }
        }
        return pawnsOfAColor;
    }

    public int movesToCompletion(Pawn.pawnColors color) {
        ArrayList<Pawn> pawns = getPawnsOfAColor(this, color);
        int totalRemaingingMoves = 0;
        for (int i = 0; i < pawns.size(); i++) {
            Pawn pawn = pawns.get(i);
            totalRemaingingMoves += pawn.movesToHome();
        }
        return totalRemaingingMoves;
    }



}
