package main.controllers;

public class SplashController extends ApplicationController {

    public static int height = 300;
    public static int width = 300;

    @Override
    public String getViewPath() {
        return "/main/views/splash.fxml";
    }

}
