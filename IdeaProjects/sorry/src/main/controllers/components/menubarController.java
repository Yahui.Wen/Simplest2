package main.controllers.components;

import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import main.controllers.ApplicationController;
import main.controllers.GameController;
import main.controllers.OpponentsController;
import main.controllers.ResumeGameController;

import java.io.IOException;

public class menubarController extends ApplicationController {

    @FXML public MenuItem fileCreateNewGame;
    @FXML public MenuItem fileResumeGame;
    @FXML public MenuItem menuItemMainMenu1;
    @FXML public MenuItem fileClose;
    @FXML public MenuItem menuItemLeaderboard;
    @FXML public MenuItem menuItemStatsPage;

    @Override
    public String getViewPath() {
        return null;
    }

    public void gotoOpponentTesting() throws IOException {
        changeView(new OpponentsController());
    }

    public void gotoGameBoardTesting() throws IOException {
        changeView(new GameController());
    }

    public void gotoResumeGame() throws IOException {
        changeView(new ResumeGameController());
    }

    public void gotoHelpPage() throws IOException {
        String urlOpen = "http://rberline.w3.uvm.edu/sorry/docs/";
        java.awt.Desktop.getDesktop().browse(java.net.URI.create(urlOpen));
    }
}
