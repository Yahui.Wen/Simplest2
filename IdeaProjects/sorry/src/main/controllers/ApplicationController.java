package main.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import main.Sorry;
import main.controllers.components.footerController;
import main.controllers.components.menubarController;

import java.io.IOException;
import java.util.concurrent.Callable;

import static java.lang.System.exit;

/**
 * This serves as the base for all of our controllers in our app. Every controller will, and should
 * extend this class. It provides helper functions for various labels (think menubar and footer) as well
 * common tasks that will be executed often in our other controllers.
 *
 * @author Andrew Hollar, Ryan Berliner, Sam Nguon, Yahui Wen
 */
public abstract class ApplicationController implements Controller{
    /**
     * These are the default dimensions of the application frame. the contentHeight is an estimation
     * of the height of the application content area, which is the application frame mins the menubar,
     * and the footer's height.
     */
    public static int defaultHeight = 800;
    public static int defaultWidth = 1200;
    public static int contentHeight = defaultHeight - 70;

    /**
     * The main stage. Everything, and every view/gui, will go in here.
     */
    public Stage window;

    /**
     * The FXML elements for the main pane (where the content goes between the header and footer) and the
     * contorllers for the menubar, and the controller.
     */
    @FXML public Pane mainPane;
    @FXML public menubarController menubarController;
    @FXML public footerController footerController;

    /**
     * Simply put, the constructors only purpose is for our reference to 'window' will be THE window that
     * the user is looking at, not some random stage object.
     */
    public ApplicationController() {
        window = Sorry.applicationWindow;
    }

    /**
     * A helper function that makes it easy to show a new view and gui. All it news is the controller for the
     * view, and voila!
     *
     * @param controller The controller of the view you'd like to show.
     * @throws IOException
     *
     * @see ApplicationController
     */
    public void changeView(ApplicationController controller) throws IOException {
        System.out.println("View: " + controller.getViewPath());
        Parent computerSelect = FXMLLoader.load(getClass().getResource(controller.getViewPath()));
        computerSelect.getStylesheets().add("/main/resources/styles/global.css");
        window.setScene(new Scene(computerSelect, ApplicationController.defaultWidth, ApplicationController.defaultHeight));
        window.setMinWidth(1050);
        window.setMinHeight(650);
        window.show();
    }

    public void closeGame() {
        System.out.println("Goodbye.");
        exit(0);
    }

    public void gotoResumeGame() throws  IOException {
        changeView(new NewGameController());
    }

    public void gotoCreateNewGame() throws IOException {
        changeView(new NewGameController());
    }

    public void gotoMainMenu() throws IOException {
        changeView(new MainMenuController());
    }

    public void showWarning(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText(null);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static void executeAfterNSeconds(int milliseconds, Callable<Void> givenFunction) {
        new java.util.Timer().schedule(
                new java.util.TimerTask(){
                    @Override
                    public void run() {
                        try {
                            givenFunction.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, milliseconds
        );
    }

}
