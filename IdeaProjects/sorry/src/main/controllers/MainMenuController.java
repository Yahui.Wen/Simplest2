package main.controllers;

import javafx.fxml.FXML;
import main.game.Pawn;

import java.awt.*;
import java.io.IOException;

public class MainMenuController extends ApplicationController {

    @FXML
    public void initialize() {
        // Update the footer to show the page name
        footerController.footerText.setText("Main Menu");
    }

    public void gotoCreateNewGame() throws IOException {
        changeView(new NewGameController());
    }

    public void gotoResumeGame() throws IOException {
        changeView(new ResumeGameController());
    }

    @Override
    public String getViewPath() {
        return "/main/views/main_menu.fxml";
    }

}