package main.controllers;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import main.Sorry;
import main.game.Board;
import main.game.BoardSpace;
import main.game.Pawn;
import main.helpers.ComputerTurn;
import main.models.CardModel;
import main.models.GameModel;
import main.models.PlayerModel;

import java.util.ArrayList;

import static main.game.BoardSpace.spaceType.*;

/**
 * The game controller handles most the of the game flow. While most of the heavy logic, computations,
 * and organizations are in models and other classes, this handles player turns, 'animation', etc of the
 * game play.

 * @author Andrew Hollar, Ryan Berliner, Sam Nguon, Yahui Wen
 */
public class GameController extends ApplicationController {
    /**
     * The main layout for this view is a split pane. The right pane is where we have the
     * game board. This side should be a square all the time. The left side will have a
     * dynamic width and will hold the game deck, and game messages, activity, etc.
     */
    @FXML public SplitPane splitPane;
    @FXML public AnchorPane rightPane;
    @FXML public AnchorPane leftPane;

    /**
     * This is the 16x16 board grid that the pawns move around on in actuality... the image of the
     * sorry board is just that, and image. The grid and the image are lined up.
     */
    @FXML public GridPane gameGrid;

    /**
     * The game information is all held on the left side of the split pane. The stretching pane
     * is set to fill all available height, and contains the the left pane v box. This contains
     * the other VBoxes which hold specific information pertaining to game play.
     */
    @FXML public VBox leftPaneVBox;
    @FXML public Pane stretchingPane;
    @FXML public VBox messagesVBox;
    @FXML public VBox playerTurnsVBox;

    /**
     * The visual representation of the game deck.
     */
    @FXML public HBox deckHBox;
    @FXML public ImageView currentCard;
    @FXML public ImageView backSideOfSorryDeck;

    /**
     * Messaging and controls for the game play.
     */
    @FXML public Text gameMessagingText;
    @FXML public Button gotoNextPlayerButton;

    /**
     * Initialize the board that we want to be available throughout the class. This way we can
     * resize, redraw, etc from multiple methods and know we always have the correct Board object.
     */
    private Board board = new Board();


    /**
     * We need to know if the active player has already drawn from the deck. That is this vars job.
     */
    private boolean hasDrawnFromDeck;

    /**
     * When a human player is moving there pawns around, we need to know what pawns are being moved.
     */
    private ArrayList<Pawn> currentPlayerPawnsInPlay = new ArrayList<>();

    private Pawn opponentPawnInPlay;

    /**
     * These classes are strictly for styling purposes. They serve not actually
     * function to the game other than aesthetics.
     */
    private static String defaultPlayerListClassName = "playerList";

    /**
     * When the game is initialized, mostly we need to make sure the elements in the view are the correct
     * size, and bound to the window dimensions. This makes the game as responsive as we can get it.
     * Once that is done, we can start preparing the game for playing. Create the pawns, draw the board,
     * position the pawns, etc. Next, randomly pick a player to go first. Once that happens, we
     * populate the player list that will show on the left of the screen and we tell the first player
     * that it is there turn. Off we go!
     */
    @FXML
    public void initialize() {
        // Bind things to be responsive.
        splitPane.prefWidthProperty().bind(mainPane.widthProperty());
        splitPane.prefHeightProperty().bind(mainPane.heightProperty());
        leftPaneVBox.prefHeightProperty().bind(mainPane.heightProperty());
        leftPaneVBox.prefWidthProperty().bind(leftPane.widthProperty());
        deckHBox.prefWidthProperty().bind(leftPane.widthProperty());
        stretchingPane.prefHeightProperty().bind(leftPane.widthProperty());
        messagesVBox.prefWidthProperty().bind(leftPane.widthProperty());
        gameMessagingText.wrappingWidthProperty().bind(leftPane.widthProperty().subtract(30));
        playerTurnsVBox.prefWidthProperty().bind(leftPane.widthProperty());
        gotoNextPlayerButton.prefWidthProperty().bind(leftPane.widthProperty());

        // When the window changes size, do this again.
        ChangeListener<Number> stageSizeListener = (observable, oldValue, newValue) -> {
            forceGameBoardToSqaureAndResizeBoard();
        };
        window.widthProperty().addListener(stageSizeListener);
        window.heightProperty().addListener(stageSizeListener);

        // Draw the board, initialize the pawns, add them to the start, and then make everything
        // the correct size.
        board.draw(gameGrid);
        Sorry.theGame.setBoard(board);
        Sorry.theGame.initializePawnsInStart();
        Sorry.theGame.addPawnsToBoard(board);
        forceGameBoardToSquareAndResizeBoard(ApplicationController.contentHeight);
        board.doResizing(ApplicationController.contentHeight / 16);

        // Pick who goes first, and save the game as in progress.
        Sorry.theGame.setGameStatus(GameModel.GameStatus.in_progress);
        Sorry.theGame.pickRandomFirstPlayer();
        Sorry.theGame.saveGame();

        // Set up events.
        gotoNextPlayerButton.setOnAction((event -> {
            Sorry.theGame.gotoNextPlayer();
            saveAllCurrentPawnsInPlay();
            clearAllCurrentPawnsInPlay();
            doTurn();
        }));
        backSideOfSorryDeck.setOnMouseClicked(event -> {
            if (Sorry.theGame.getCurrentPlayer().getPlayerId() == 0) {
                return;
            }
            humanDrawFromDeck();
        });

        // Add events to each of the pawns.
        for (int i = 0; i < board.getBoardSpaces().size(); i++) {
            for (int j = 0; j < board.getBoardSpaces().get(i).getPawns().size(); j++) {
                board.getBoardSpaces().get(i).getPawns().get(j).setOnMouseClicked((event -> {
                    if (Sorry.theGame.getCurrentPlayer().getPlayerId() == 0) {
                        showWarning("Not Allowed", "It is not your turn.");
                        return;
                    }
                    if (!isHasDrawnFromDeck()) {
                        showWarning("Not Allowed", "You need to draw from the deck before you can move pawns.");
                        return;
                    }
                    currentPlayerPawnsInPlay.add((Pawn) event.getTarget());
                    if (event.getButton() == MouseButton.SECONDARY) {
                        rightClickAPawn((Pawn) event.getTarget());
                    } else {
                        leftClickAPawn((Pawn) event.getTarget());
                    }
                }));
            }
        }
        // Populate the players on the left, then do the turn for the first player. Off we go!
        populatePlayerList();
        startGame();
    }

    /**
     * The messaging for the first players turn will be a little bit different then normal, so here
     * we show print out that message and show the indicator so they know who needs to go first.
     */
    public void startGame() {
        PlayerModel firstPlayer = Sorry.theGame.getCurrentPlayer();
        updateGameMessage(firstPlayer.getPlayerName() + " let's get this game going! You were randomly selected to go first!");
        doTurn();
    }

    /**
     * This is called whenever it is someones turn, regardless of the type of player or whether they have
     * already finished the game. It is the responsibility of this method to deligate tasks based on the type
     * of player, and there placement in the game (finished or still playing). Once it is done delegating
     * events, it will save the game. This means that when a player is done with there turn, the game is
     * automatically saved.
     *
     * <b>Note: </b> We don't have any messaging in this method, because that will be the task of whatever
     * methods this function delegates to. That's because of how situational the messaging will be.
     * <b>Note: </b> The computer turn, and human turn functions must be sure that they set the is in middle
     * of turn var to false so that the next player button is disabled.
     */
    public void doTurn() {
        setHasDrawnFromDeck(false);
        showCurrentPlayerIndicator();
        PlayerModel currentPlayer = Sorry.theGame.getCurrentPlayer();
        if (currentPlayer.getPlayerId() == 0) {
            computerTurn();
        } else {
            humanTurn();
        }
        // If the game deck is empty, let's update it before the next turn and before saving the game.
        if (Sorry.theGame.getGameDeck().getCards().size() == 0) {
            Sorry.theGame.getGameDeck().deckRefresh();
            Sorry.theGame.getGameDeck().cardReshuffle();
        }
        Sorry.theGame.saveGame();
    }

    /**
     * If it is the computers turn to make a move, this function will be called to make the magic
     * happen. The last function call in here should make sure that it sets the is in middle of turn
     * var to false, because the computer is now done with what they are doing.
     */
    public void computerTurn() {
        PlayerModel currentPlayer = Sorry.theGame.getCurrentPlayer();
        drawFromDeck();
        ArrayList<ComputerTurn> allPossibleMoves = Board.calculateAllMoves(Sorry.theGame);
        if (allPossibleMoves.size() != 0) {
            System.out.println("Here are all the possible moves:");
            System.out.println("---------------------------------------------------------------------------------");
            for (int i = 0; i < allPossibleMoves.size(); i++) {
                ComputerTurn turn = allPossibleMoves.get(i);
                System.out.println(turn.toString());
            }
            System.out.println("---------------------------------------------------------------------------------");
            ComputerTurn computerTurn = currentPlayer.pickAMove(allPossibleMoves);


            boolean found = false;

            for (int i1 = 0; i1 < board.getBoardSpaces().size(); i1++) {
                BoardSpace space = board.getBoardSpaces().get(i1);
                for (int j = 0; j < space.getPawns().size(); j++) {
                    // Look for pawns to move back home.
                    for (int i = 0; i < computerTurn.getAffectedPawnIds().size(); i++) {
                        Pawn pawnToSendHome = space.getPawns().get(j);
                        if (pawnToSendHome.getPawnId() == computerTurn.getAffectedPawnIds().get(i)) {
                            pawnToSendHome.goHome();
                            pawnToSendHome.savePawnPosition();
                        }
                    }
                    // Look for the opponent pawn to move.
                    if (!space.getPawns().isEmpty() && space.getPawns().get(0).getPawnId() == computerTurn.getPawnId()) {
                        Pawn thePawnToMove = space.getPawns().get(j);
                        if (!found) {
                            // TODO: 4/25/18 We need to account for reverse moves and swaps.
                            if (computerTurn.getMoves() < 0) {
                                System.out.println("MOVING BACKWARDS: " + computerTurn.getMoves());
                                for (int k = 0; k < Math.abs(computerTurn.getMoves()); k++) {
                                    thePawnToMove.gotoPreviousPlace();
                                    found = true;
                                }
                                thePawnToMove.savePawnPosition();
                            } else {
                                System.out.println("MOVING FORWARDS: " + computerTurn.getMoves());
                                for (int k = 0; k < computerTurn.getMoves(); k++) {
                                    thePawnToMove.gotoNextPlace();
                                    found = true;
                                }
                                thePawnToMove.savePawnPosition();
                            }
                        }
                    }
                }
            }
        }
        executeAfterNSeconds( 0, () -> {
            String newMessage = currentPlayer.getPlayerName() + " drew a " + Sorry.theGame.getGameDeck().getCurrentCard().toString() + ".\n";
            newMessage += "They are figuring out what they will do...";
            updateGameMessage(newMessage);
            return null;
        });
        executeAfterNSeconds( 1000, () -> {
            String newMessage = currentPlayer.getPlayerName() + " has completed their turn.";
            if (allPossibleMoves.size() == 0) {
                newMessage = "There are no valid moves.";
            }
            updateGameMessage(newMessage);
            return null;
        });
    }

    /**
     * The human player has to take there turn, and this will prompt them with instructions, options, etc.
     */
    public void humanTurn() {
        PlayerModel currentPlayer = Sorry.theGame.getCurrentPlayer();
        updateGameMessage(currentPlayer.getPlayerName() + ", your turn. Click the deck to draw a card.");
    }

    /**
     * When a human player draws from the deck, we will need to execute some logic and do some delegation
     * based on the card that they drew.
     */
    public void humanDrawFromDeck() {
        if (Sorry.theGame.getCurrentPlayer().getPlayerId() != 0 && !isHasDrawnFromDeck()) {
            drawFromDeck();
            setHasDrawnFromDeck(true);
            String message = "You drew a " + Sorry.theGame.getGameDeck().getCurrentCard().toString() + ".\n";
            message += "Left click a pawn to move it forward, right click for backwards. To swap, click the " +
                    "opponents pawn you like to swap with, and then your pawn.";
            updateGameMessage(message);
        } else {
            showWarning("Not Allowed", "You already drew a card from the deck.");
        }
    }

    /**
     * Pawns are moved by clicking them, and left clicking the pawn will move it forward (or select it for a swap).
     */
    public void leftClickAPawn(Pawn pawn) {
        if (pawn.getColor() != Sorry.theGame.getCurrentPlayer().getPawnColor()) {
            // Here we try and set the pawn to swap (the opponents pawn).
            leftClickAnOpponentPawn(pawn);
        } else {
            leftClickOwnPawn(pawn);
        }
    }


    public void leftClickAnOpponentPawn(Pawn pawn) {
        CardModel currentCard = Sorry.theGame.getGameDeck().getCurrentCard();
        if (!currentCard.isPositionSwappable()) {
            showWarning("Not Allowed", "You cannot swap pawn positions with a " + currentCard.toString() + ".");
            return;
        } else {
            if (opponentPawnInPlay != null) {
                opponentPawnInPlay.unHighlightPawn();
            }
            if (opponentPawnInPlay == pawn) {
                pawn.unHighlightPawn();
                opponentPawnInPlay = null;
                return;
            }
            opponentPawnInPlay = pawn;
            pawn.highlightPawn();
        }
    }

    /**
     * Left clicking your own pawn should try and move it forward.
     * @param pawn
     */
    public void leftClickOwnPawn(Pawn pawn) {
        CardModel card = Sorry.theGame.getGameDeck().getCurrentCard();
        if (pawn.getBoardSpace().isStartSpace() && !card.isMoveFromStart() && card.getCardNumber() != 0) {
            showWarning("Not Allowed", "You cannot move a pawn from start with a " + card.toString() + ".");
            return;
        }
        if (opponentPawnInPlay != null) {
            BoardSpace opponentSpace = opponentPawnInPlay.getBoardSpace();
            BoardSpace originionalSpace = pawn.getBoardSpace();
            if (card.getCardNumber() == 0) {
                opponentPawnInPlay.goHome();
            } else {
                opponentPawnInPlay.setBoardSpace(originionalSpace);
            }
            pawn.setBoardSpace(opponentSpace);
            return;
        }
        if (pawn.canGoForwards()) {
            pawn.gotoNextPlace();
        } else {
            showWarning("Not Allowed", "There is no where to go forwards.");
        }
    }

    /**
     * Right clicking on pawn will move it backwards if applicable.
     */
    public void rightClickAPawn(Pawn pawn) {
        if (!pawn.canGoBackwards()) {
            showWarning("Not Allowed", "There is no where to go backwards.");
            return;
        }
        if (pawn.getColor() != Sorry.theGame.getCurrentPlayer().getPawnColor()) {
            pawn.goHome();
            return;
        }
        pawn.gotoPreviousPlace();
    }

    /**
     * When the view is initialized, we need ot populate the list of players that will show
     * on the left of the screen. Voila. This basically just loops through the players, and adds them
     * to the players v box. The only thing that is of note, and not what you might expect, is that
     * we also set the name of the computer players in this function. That way during gameplay we can
     * get the players name the same regardless of if they are a computer or a person.
     */
    public void populatePlayerList() {
        int computerPlayerCount = 1;
        ArrayList<PlayerModel> players = Sorry.theGame.getPlayers();
        for (int i = 0; i < players.size(); i++) {
            PlayerModel player = players.get(i);
            String playerName = "Computer " + computerPlayerCount;
            if (player.getPlayerId() == 0) {
                computerPlayerCount++;
                player.setPlayerName(playerName);
                // TODO: 4/22/18 Save the player with the new name? Actually might not be nesescary... think about it...
            } else {
                playerName = player.getPlayerName();
            }
            HBox singlePlayerHBox = new HBox();
            Label playerLabel = new Label(playerName);
            singlePlayerHBox.getStyleClass().add(defaultPlayerListClassName);
            singlePlayerHBox.prefWidthProperty().bind(leftPaneVBox.prefWidthProperty());
            singlePlayerHBox.getChildren().add(playerLabel);
            String color = "";
            switch (player.getPawnColor()) {
                case Red:
                    color = "#A00003";
                    break;
                case Blue:
                    color = "#2D5FBE";
                    break;
                case Green:
                    color = "#45831E";
                    break;
                case Yellow:
                    color = "#C4C008";
                    break;
            }
            singlePlayerHBox.setStyle("-fx-border-color: " + color);
            playerTurnsVBox.getChildren().add(singlePlayerHBox);
        }
    }

    /**
     * A wrapper function around forceGameBoardToSquareAndResizeBoard() that will use the REAL height
     * of the content area to resize everything. Also will resize the board (the size of the baordspaces).
     */
    public void forceGameBoardToSqaureAndResizeBoard() {
        forceGameBoardToSquareAndResizeBoard(rightPane.getHeight());
        board.resize(gameGrid);
    }

    /**
     * When given an estimated height of the content area, this will force the board area to be square, and
     * size the pawns within the board to be proportionate with the size of the baord game grid.
     *
     * @param estimateHeight The estimated height of the content area in pixels.
     */
    public void forceGameBoardToSquareAndResizeBoard(double estimateHeight) {
        rightPane.setMinWidth(estimateHeight);
        rightPane.setMaxWidth(estimateHeight);
        // Size the pawns as well...
        board.sizePawns(estimateHeight / 16);
    }

    /**
     * This is a wrapper function around the deck 'drawFromDeck()' that incorporates javafx to
     * display the image of the card on the screen.
     */
    public void drawFromDeck(){
        Sorry.theGame.getGameDeck().drawFromDeck();
        currentCard.setImage(Sorry.theGame.getGameDeck().getCurrentCard().getCardImage());
        currentCard.setViewport(Rectangle2D.EMPTY);
        currentCard.setVisible(true);
    }

    /**
     * There is a section on the left side fo the screen for messages to the person playing the game.
     * This function provides an easy way to change that message.
     *
     * @param message The message to show.
     */
    public void updateGameMessage(String message) {
        gameMessagingText.setText(message);
    }

    /**
     * Give a visual flag next to the active player on the left side of the screen so you can know
     * which player is the current player (whose turn it is).
     */
    public void showCurrentPlayerIndicator() {
        // Make sure there aren't any current indicators already.
        for (int i = 0; i < playerTurnsVBox.getChildren().size(); i++) {
            HBox inspectedPlayerHBox = (HBox) playerTurnsVBox.getChildren().get(i);
            inspectedPlayerHBox.getStyleClass().remove("current");
        }
        PlayerModel currentPlayer = Sorry.theGame.getCurrentPlayer();
        int indexOfCurrentPlayer = Sorry.theGame.getPlayers().indexOf(currentPlayer);
        HBox currentPlayerHBOX = (HBox) playerTurnsVBox.getChildren().get(indexOfCurrentPlayer); // bad.
        currentPlayerHBOX.getStyleClass().add("current");
    }

    /**
     * Checks whether or not the current player has draw from the deck.
     *
     * @return Whether or not the deck has been drawn from by the current player.
     */
    public boolean isHasDrawnFromDeck() {
        return hasDrawnFromDeck;
    }

    /**
     * Change the value of whether or not the current player has already drawn from the deck.
     *
     * @param hasDrawnFromDeck Whether they have drawn from the deck.
     */
    public void setHasDrawnFromDeck(boolean hasDrawnFromDeck) {
        this.hasDrawnFromDeck = hasDrawnFromDeck;
    }


    /**
     * When a humans turn is over, we should clear the current player pawns in play so that the
     * next time it is there turn everything is reset. This function clears current pawns in play.
     */
    public void clearAllCurrentPawnsInPlay() {
        currentPlayerPawnsInPlay.clear();
    }

    public void saveAllCurrentPawnsInPlay() {
        for (int j = 0; j < currentPlayerPawnsInPlay.size(); j++) {
            currentPlayerPawnsInPlay.get(j).savePawnPosition();
        }
    }

    @Override
    public String getViewPath() {
        return "/main/views/game.fxml";
    }
}
