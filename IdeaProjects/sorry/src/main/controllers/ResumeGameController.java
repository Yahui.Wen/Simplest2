package main.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import main.Sorry;
import main.models.GameModel;
import main.game.Pawn;
import javafx.scene.control.TextField;
import main.models.PlayerModel;

import java.io.IOException;

public class ResumeGameController extends ApplicationController {

    @FXML private TextField gameName;
    @FXML private TextField username;
    @FXML private ChoiceBox selection;

    @FXML
    public void initialize() {

    }

    public boolean validation(){
        if (username.getText().isEmpty() || gameName.getText().isEmpty()) {
            showWarning("Validate fields", "Please enter your game name");
            return false;
        } else {
            return true;
        }
    }

    public void resume() throws IOException {
        if (!validation()) {
            return;
        }

    }

    @Override
    public String getViewPath() {
        return "/main/views/resume_game.fxml";
    }
}
