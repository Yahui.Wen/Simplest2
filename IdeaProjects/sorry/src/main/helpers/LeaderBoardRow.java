package main.helpers;

import javafx.beans.property.SimpleStringProperty;

public class LeaderBoardRow {

    public Integer place;
    public SimpleStringProperty playerName;

    public LeaderBoardRow(int place, String playerName) {
        this.place = new Integer(place);
        this.playerName = new SimpleStringProperty(playerName);
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public String getPlayerName() {
        return playerName.get();
    }

    public SimpleStringProperty playerNameProperty() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName.set(playerName);
    }

}