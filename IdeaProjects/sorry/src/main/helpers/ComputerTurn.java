package main.helpers;

import main.controllers.ApplicationController;
import main.controllers.GameController;
import main.game.Board;
import main.game.BoardSpace;
import main.game.Pawn;
import main.models.GameModel;
import main.models.PlayerModel;

import java.util.ArrayList;


public class ComputerTurn {
    int pawnId;
    int moves;
    ArrayList<Integer> affectedPawnIds = new ArrayList<>();

    int startMovesToCompletion;
    int endMovesToCompletion;
    int startOpponentMovesToCompletion;
    int endOpponentMovesToCompletion;

    boolean didMoveFromStart;
    boolean didFinishPawn;
    boolean didSafetyPawn;


    public ComputerTurn(GameModel game) {
        // TODO: 4/24/18 Populate start moves to completion and end moves from the game opject.
        // See how many nextPlaces it takes for the current player to go home.
        setMovesToCompletion(game);
        setDidMoveFromStart(false);
        setDidFinishPawn(false);
        setDidSafetyPawn(false);
    }

    public void setMovesToCompletion(GameModel game) {
        int totalPersonalMoves = game.getBoard().movesToCompletion(game.getCurrentPlayer().getPawnColor());
        int opponentsMovesToCompletion = 0;
        for (int i = 0; i < game.getPlayers().size(); i++) {
            PlayerModel player = game.getPlayers().get(i);
            if (player == game.getCurrentPlayer()) {
                continue;
            }
            opponentsMovesToCompletion += game.getBoard().movesToCompletion(player.getPawnColor());
        }
        setStartOpponentMovesToCompletion(opponentsMovesToCompletion);
        setStartMovesToCompletion(totalPersonalMoves);
    }

    public int getPawnId() {
        return pawnId;
    }

    public void setPawnId(int pawnId) {
        this.pawnId = pawnId;
    }

    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }

    public ArrayList<Integer> getAffectedPawnIds() {
        return affectedPawnIds;
    }

    public void addAffectedPawnId(int affectedPawnId) {
        this.affectedPawnIds.add(affectedPawnId);
    }

    public int getStartMovesToCompletion() {
        return startMovesToCompletion;
    }

    public void setStartMovesToCompletion(int startMovesToCompletion) {
        this.startMovesToCompletion = startMovesToCompletion;
    }

    public int getEndMovesToCompletion() {
        return endMovesToCompletion;
    }

    public void setEndMovesToCompletion(int endMovesToCompletion) {
        this.endMovesToCompletion = endMovesToCompletion;
    }

    public int getStartOpponentMovesToCompletion() {
        return startOpponentMovesToCompletion;
    }

    public void setStartOpponentMovesToCompletion(int startOpponentMovesToCompletion) {
        this.startOpponentMovesToCompletion = startOpponentMovesToCompletion;
    }

    public int getEndOpponentMovesToCompletion() {
        return endOpponentMovesToCompletion;
    }

    public void setEndOpponentMovesToCompletion(int endOpponentMovesToCompletion) {
        this.endOpponentMovesToCompletion = endOpponentMovesToCompletion;
    }

    public boolean isDidMoveFromStart() {
        return didMoveFromStart;
    }

    public void setDidMoveFromStart(boolean didMoveFromStart) {
        this.didMoveFromStart = didMoveFromStart;
    }

    public boolean isDidFinishPawn() {
        return didFinishPawn;
    }

    public void setDidFinishPawn(boolean didFinishPawn) {
        this.didFinishPawn = didFinishPawn;
    }

    public boolean isDidSafetyPawn() {
        return didSafetyPawn;
    }

    public void setDidSafetyPawn(boolean didSafetyPawn) {
        this.didSafetyPawn = didSafetyPawn;
    }

    public void execute(Board board) {
        // Find the pawn by the id.
//        for (int i = 0; i < board.getBoardSpaces().size(); i++) {
//            BoardSpace space = board.getBoardSpaces().get(i);
//            for (int j = 0; j < space.getPawns().size(); j++) {
//                if (space.getPawns().get(j).getPawnId() == getPawnId()) {
//                    Pawn thePawnToMove = space.getPawns().get(j);
//                    // TODO: 4/25/18 We need to account for reverse moves and swaps.
//                    for (int k = 0; k < getMoves(); k++) {
//                        ApplicationController.executeAfterNSeconds(k * 100, () -> {
//                            thePawnToMove.gotoNextPlace();
//                            return null;
//                        });
//                    }
//                    thePawnToMove.savePawnPosition();
//                }
//            }
//        }
    }

    @Override
    public String toString() {
        return "Pawn ID: " + getPawnId() + " can move.";
    }
}
