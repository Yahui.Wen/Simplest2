package main.models;

import main.game.Board;
import main.game.Pawn;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;
import main.game.Pawn.pawnColors;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GameModel {

    private int gameId;
    private GameStatus gameStatus;
    private String gameName;
    private DeckModel gameDeck;
    private int numOpponents;
    private int gamePlayerIdActive;
    private Board board;
    
    private ArrayList<PlayerModel> players = new ArrayList<>();

    public enum GameStatus {
        setting_up, in_progress, complete
    }

    /**
     * It will order players based on there pawn color, moving from Blue -> Yellow -> Green -> Red
     * Which is clockwise around the game board.
     */
    public void orderPlayers() {
        // Blue, Yellow, Green, Red
        ArrayList<PlayerModel> newPLayerList = new ArrayList<>();
        for (int i = 0; i < getPlayers().size(); i++) {
            if (getPlayers().get(i).getPawnColor() == pawnColors.Blue) {
                newPLayerList.add(getPlayers().get(i));
                break;
            }
        }
        for (int i = 0; i < getPlayers().size(); i++) {
            if (getPlayers().get(i).getPawnColor() == pawnColors.Yellow) {
                newPLayerList.add(getPlayers().get(i));
                break;
            }
        }
        for (int i = 0; i < getPlayers().size(); i++) {
            if (getPlayers().get(i).getPawnColor() == pawnColors.Green) {
                newPLayerList.add(getPlayers().get(i));
                break;
            }
        }
        for (int i = 0; i < getPlayers().size(); i++) {
            if (getPlayers().get(i).getPawnColor() == pawnColors.Red) {
                newPLayerList.add(getPlayers().get(i));
                break;
            }
        }
        setPlayers(newPLayerList);
    }

    /**
     * This will pick the first player by setting the gamePlayerIdActive to a random player in the list.
     */
    public void pickRandomFirstPlayer() {
        Random r = new Random();
        PlayerModel player = players.get(r.nextInt(((getPlayers().size() - 1)) + 1));
        setGamePlayerIdActive(player.getGamePlayerId());
    }

    /**
     * Looks up the current player in the player ArrayList by the gamePlayerIdActive.
     *
     * @return The current player.
     * @see PlayerModel
     */
    public PlayerModel getCurrentPlayer() {
        int currentPlayerIndex = 0;
        for (int i = 0; i < getPlayers().size(); i++) {
            if (getPlayers().get(i).getGamePlayerId() == getGamePlayerIdActive()) {
                currentPlayerIndex = i;
                break;
            }
        }
        return getPlayers().get(currentPlayerIndex);
    }

    /**
     * This will go to the next player in the player list. The function should be called to rotate
     * to the next person after someone completes there turn. It will return the next player, and set
     * the gamePlayerIdActive in the process. That way you can save the game and you good to go.
     *
     * @return The next player, which is the new current player.
     * @see PlayerModel
     */
    public PlayerModel gotoNextPlayer() {
        // Lets get the index of the current player, and go to the next one if we can. If not
        // than we loop back to the beginning.
        int currentPlayerIndex = players.indexOf(getCurrentPlayer());
        int nextPlayerIndex = (currentPlayerIndex == getPlayers().size() - 1) ? 0 : currentPlayerIndex + 1;
        setGamePlayerIdActive(getPlayers().get(nextPlayerIndex).getGamePlayerId());
        return getPlayers().get(nextPlayerIndex);
    }

    public boolean createNewGame(String gameName) {
        SorryAPIResponse response = SorryAPI.createGame(gameName);
        if (response.isSuccess()) {
            int gameId = response.getData().get("id").getAsInt();
            setGameId(gameId);
            DeckModel deck = new DeckModel(this);
            setGameDeck(deck);
            getGameDeck().cardReshuffle();
            return saveGame();
        } else {
            return false;
        }
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public DeckModel getGameDeck() {
        return gameDeck;
    }

    public void setGameDeck(DeckModel gameDeck) {
        this.gameDeck = gameDeck;
    }

    public int getNumOpponents() {
        return numOpponents;
    }

    public void setNumOpponents(int numOpponents) {
        this.numOpponents = numOpponents;
    }

    public ArrayList<PlayerModel> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<PlayerModel> players) {
        this.players = players;
    }

    public void addPlayer(PlayerModel player) {
        ArrayList<PlayerModel> newPlayers = getPlayers();
        newPlayers.add(player);
        setPlayers(getPlayers());
    }

    public void initializePawnsInStart() {

    }

    public void addPawnsToBoard(Board board) {
        for (int i = 0; i < getPlayers().size(); i++) {
            PlayerModel player = getPlayers().get(i);
            for (int j = 0; j < 4; j++) {
                board.addPawnToBoard(new Pawn(player, board));
            }
        }
    }

    public int getGamePlayerIdActive() {
        return gamePlayerIdActive;
    }

    public void setGamePlayerIdActive(int gamePlayerIdActive) {
        this.gamePlayerIdActive = gamePlayerIdActive;
    }

    public boolean saveGame() {
        SorryAPIResponse response = SorryAPI.saveGame(this);
        return response.isSuccess();
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
}
