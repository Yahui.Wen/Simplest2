package main.models;

import java.util.ArrayList;
import java.util.Collections;

public class DeckModel extends ApplicationModel {

    private GameModel game;
    private ArrayList<CardModel> cards = new ArrayList<>();
    private CardModel currentCard;

    // The index of the array corresponds to the card number. NOTE: no 6's or 9's. 0 is a 'Sorry Card'.
    public static int[] numbersOfEachCard = new int[] {4, 5, 4, 4, 4, 4, 0, 4, 4, 0, 4, 4, 4};

    public DeckModel(GameModel game) {
        this.game = game;
        deckRefresh();
    }

    public GameModel getGame() {
        return game;
    }

    public void setGame(GameModel game) {
        this.game = game;
    }

    public ArrayList<CardModel> getCards() {
        return cards;
    }

    public void setCards(ArrayList<CardModel> cards) {
        this.cards = cards;
    }

    public CardModel getCurrentCard() {
        return currentCard;
    }

    public void setCurrentCard(CardModel currentCard) {
        this.currentCard = currentCard;
    }

    public void cardReshuffle() {
        Collections.shuffle(cards);
    }

    public CardModel drawFromDeck() {
        setCurrentCard(cards.get(0));
        cards.remove(0);
        return getCurrentCard();
    }

    @Override
    public String toString() {
        StringBuilder deck = new StringBuilder();
        for (int i = 0; i < cards.size(); i++) {
            deck.append(cards.get(i).getCardNumber());
            if (i != cards.size() - 1) {
                deck.append(',');
            }
        }
        return deck.toString();
    }

    public void deckRefresh() {
        cards.clear();
        for (int i = 0; i < numbersOfEachCard.length; i++) {
            int numberOfCards = numbersOfEachCard[i];
            for (int j = 0; j < numberOfCards; j++) {
                cards.add(new CardModel(i));
            }
        }
    }

}
