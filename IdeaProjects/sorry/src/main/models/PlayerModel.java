package main.models;

import main.game.Pawn.pawnColors;
import main.helpers.ComputerTurn;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static main.models.PlayerModel.MoveQualityIndicators.*;

/**
 * The PlayerModel contains all the information about a player, as well as the logic to pick a move that
 * best suites there ability and attitude (when we are talking about computer players.
 *
 * @author Andrew Hollar, Ryan Berliner, Sam Nguon, Yahui Wen
 */
public class PlayerModel extends ApplicationModel {
    /**
     * The computer ability describes a computer players smartness/experience with the game.
     */
    public enum ComputerAbility {
        Smart, Naive
    }

    /**
     * How is the computer player feeling? These classify there emotion which dictate the kinds
     * of moves that they pick over others.
     */
    public enum ComputerFeels {
        Mean, Nice
    }

    /**
     * Each possible player move will have a certain number of each of these move quality indicators.
     * Depending on the level of ability and their attitude, comptuer players will favor each indicator
     * differently.
     */
    public enum MoveQualityIndicators {
        Start, Finish, Safety, Displace, Progress, Defense
    }

    /**
     * These multipliers give weight to difference possible moves a computer player will make.
     * The names purposefully contain the .toString() of the ComputerAbility, ComputerFeels
     * and MoveQualityInidicators enums so we can write smart functions for accessing them.
     *
     * @see MoveQualityIndicators
     */
    public static Map<String, Double> abilityMultipliers = new HashMap<>();
    public static Map<String, Double> feelsMultiplier = new HashMap<>();
    static {
        abilityMultipliers.put("SmartStart", 0.1);
        abilityMultipliers.put("NaiveStart", 0.025);
        abilityMultipliers.put("SmartFinish", 0.02);
        abilityMultipliers.put("NaiveFinish", 0.7);
        abilityMultipliers.put("SmartSafety", 0.1);
        abilityMultipliers.put("NaiveSafety", 0.025);
        abilityMultipliers.put("SmartDisplace", 0.0);
        abilityMultipliers.put("NaiveDisplace", 0.025);
        abilityMultipliers.put("SmartProgress", 0.39);
        abilityMultipliers.put("NaiveProgress", 0.2);
        abilityMultipliers.put("SmartDefense", 0.39);
        abilityMultipliers.put("NaiveDefense", 0.025);
        feelsMultiplier.put("MeanStart", 0.05);
        feelsMultiplier.put("NiceStart", 0.02);
        feelsMultiplier.put("MeanFinish", 0.05);
        feelsMultiplier.put("NiceFinish", 0.2);
        feelsMultiplier.put("MeanSafety", 0.1);
        feelsMultiplier.put("NiceSafety", 0.02);
        feelsMultiplier.put("MeanDisplace", 0.45);
        feelsMultiplier.put("NiceDisplace", 0.0);
        feelsMultiplier.put("MeanProgress", 0.05);
        feelsMultiplier.put("NiceProgress", 0.2);
        feelsMultiplier.put("MeanDefense", 0.3);
        feelsMultiplier.put("NiceDefense", 0.2);
    }

    /**
     * These all line up with field in the database, which are a compination of fields in the
     * player table, and the game player table. It makes sense that we combine them into one class here.
     *
     * Player Table */
    private int playerId;
    private String playerName;

    /* Game Player Table */
    private int gamePlayerId;
    private int place;
    private GameModel game;
    private pawnColors pawnColor;
    private ComputerFeels feels;
    private ComputerAbility ability;

    /**
     * This constructor is used for creating a human player. The server side code will check if this user
     * already exists. Regardless, the id of the player and game player will be returned so we can populate
     * those fields in the PlayerModel object.
     *
     * @param playerName The name of the player.
     * @param game The game object, used for the id.
     * @param pawnColor The color pawn of the player in the game.
     */
    public PlayerModel(String playerName, GameModel game, pawnColors pawnColor) {
        SorryAPIResponse response = SorryAPI.createHumanPlayer(playerName, game, pawnColor);
        if (response.isSuccess()) {
            setPlayerName(playerName);
            setGame(game);
            setPawnColor(pawnColor);
            setPlayerId(response.getData().get("player_id").getAsInt());
            setGamePlayerId(response.getData().get("game_player_id").getAsInt());
        }
    }

    /**
     * This constructor is used ofr creating a computer player. Given the ability, feels, and the game
     * the computer wants to join, it shoots this info off to the Sorry! API and that handles the rest.
     * It randomly assigns an available pawn color, and when it gets all the information back from the
     * database it populates the PlayerModel object appropriately.
     *
     * @param ability The ability of the new computer player.
     * @param feels The feels of the new computer player.
     * @param game The game the new computer player would like to join.
     */
    public PlayerModel(ComputerAbility ability, ComputerFeels feels, GameModel game) {
        SorryAPIResponse response = SorryAPI.createComputerPlayer(game, ability, feels);
        if (response.isSuccess()) {
            String colorFromDB = response.getData().get("pawn_color").getAsString();
            setPawnColor(pawnColors.valueOf(colorFromDB));
            setAbility(ability);
            setFeels(feels);
            setGamePlayerId(response.getData().get("game_player_id").getAsInt());
            setGame(game);
            setPlayerId(0); // This is a flag for being a computer player.
        }
    }

    /**
     * Get the game the player is a part of.
     *
     * @return The game the player is a part of.
     * @see GameModel
     */
    public GameModel getGame() {
        return game;
    }

    /**
     * Sets the game the player is apart of. This is only in the local object, no database changes.
     *
     * @param game The game the player should be apart of.
     */
    public void setGame(GameModel game) {
        this.game = game;
    }

    /**
     * Gets the id of the player. This is the database id, and will be 0 for computer players.
     *
     * @return The id of the player.
     */
    public int getPlayerId() {
        return playerId;
    }

    /**
     * Sets the id of the player, this is a local object change, no database changes. Should only
     * be used when we first get the id from the database, then can set it.
     *
     * @param playerId The id of the player.
     */
    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    /**
     * The game player is is the id of the record in the database that links a player with a game.
     *
     * @return The id of the game player record in the db.
     */
    public int getGamePlayerId() {
        return gamePlayerId;
    }

    /**
     * Sets the game player id, local object change, no database changes.
     *
     * @param gamePlayerId The id of the game player entry.
     */
    public void setGamePlayerId(int gamePlayerId) {
        this.gamePlayerId = gamePlayerId;
    }

    /**
     * Gets the name of the player. This will return nothing form computer players until the game has been
     * completely set up. For these players they don't actually have a name in the database, just locally.
     *
     * @return The name of the player.
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Sets the name of the player.
     *
     * @param playerName The name of the player.
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * Gets the computer ability of a player. For human players this won't return anything.
     *
     * @return The computer ability of the player if the player is a computer.
     */
    public ComputerAbility getAbility() {
        return ability;
    }

    /**
     * Sets the computer ability of a player. For human players this isn't ever applicable.
     *
     * @param ability The computer ability of the computer player.
     */
    public void setAbility(ComputerAbility ability) {
        this.ability = ability;
    }

    /**
     * Gets the computer feels of a player. For human players this won't return anything.
     *
     * @return The computer feels of the player if the player is a computer.
     */
    public ComputerFeels getFeels() {
        return feels;
    }

    /**
     * Sets the computer feels of a player. For human players this isn't ever applicable.
     *
     * @param feel The computer feels of the computer player.
     */
    public void setFeels(ComputerFeels feel) {
        this.feels = feel;
    }

    /**
     * Gets the place of the player. This is the placing of how they finished the game. The winner is place 1,
     * second place is 2, etc. If the Player has not finished yet this will return nothing.
     *
     * @return The place of the player.
     */
    public int getPlace() {
        return place;
    }

    /**
     * Sets the place of a player. As players finish, set there place so we know who won, can in second, etc.
     *
     * @param place The place the player finished in.
     */
    public void setPlace(int place) {
        this.place = place;
    }

    /**
     * Gets the pawn color of the player.
     *
     * @return The pawn color of the player.
     */
    public pawnColors getPawnColor() {
        return pawnColor;
    }

    /**
     * Sets the pawn color of the player.
     *
     * @param pawnColor The pawn color of the player.
     */
    public void setPawnColor(pawnColors pawnColor) {
        this.pawnColor = pawnColor;
    }

    /**
     * Given a list of possible turns that a player could take, this function will return the one that best
     * suites the players ability, and feeling. This only works for computer players.
     *
     * @param allPossibleTurns An array list of computer turns to pick from.
     *
     * @return The turn/move that best suite this player.
     */
    public ComputerTurn pickAMove(ArrayList<ComputerTurn> allPossibleTurns) {
        int highestScoreMoveIndex = 0;
        double highestScore = 0;
        for (int i = 0; i < allPossibleTurns.size(); i++) {
            ComputerTurn turn = allPossibleTurns.get(i);
            // Get the value of each move quality indicator.
            double start = (turn.isDidMoveFromStart()) ? 1 * getMultiplier(Start) : 0;
            double finish = (turn.isDidFinishPawn()) ? 1 * getMultiplier(Finish) : 0;
            double safety = (turn.isDidSafetyPawn()) ? 1 * getMultiplier(Safety) : 0;
            double displace = turn.getAffectedPawnIds().size() * getMultiplier(Displace);
            double progress = (turn.getStartMovesToCompletion() - turn.getEndMovesToCompletion()) * getMultiplier(Progress);
            double defense = (turn.getStartOpponentMovesToCompletion() - turn.getStartOpponentMovesToCompletion()) * getMultiplier(Defense);
            // Add them all up, see how it compares.
            double score = start + finish + safety + displace + progress + defense;
            if (score > highestScore) {
                highestScore = score;
                highestScoreMoveIndex = i;
            }
        }
        return allPossibleTurns.get(highestScoreMoveIndex);
    }

    /**
     * This get the multiplier of a particutalar move quality indicator for the player based on there
     * ability, and there feels. It acts as a weighted number that will help pick the most appropriate
     * move.
     *
     * @param indicator The move quality indicator you want to get the multiplier for.
     *
     * @return The multiplier for the given indicator for the player.
     */
    public double getMultiplier(MoveQualityIndicators indicator) {
        // This is why we used the enum names in the multipliers at the top. ;)
        double abilityMultiplierValue = abilityMultipliers.get(getAbility().toString() + indicator.toString());
        double feelsMultiplierValue = feelsMultiplier.get(getFeels().toString() + indicator.toString());
        return abilityMultiplierValue * feelsMultiplierValue;
    }
}



