package test.models;

import main.game.Pawn;
import main.helpers.SorryAPI;
import main.helpers.SorryAPIResponse;
import main.models.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SorryAPITest {

    @Test
    void updatePawnPosition() {
        GameModel game = new GameModel();
        game.createNewGame("[TEST] Created from SorryAPITest.updatePawnPosition()");
        PlayerModel player = new PlayerModel("[TEST] Created from SorryAPITest.updatePawnPosition()", game, Pawn.pawnColors.Blue);
        SorryAPIResponse response1 = SorryAPI.createPawn(player.getGamePlayerId(), 0, 0);
        if (!response1.isSuccess()) {
            assertEquals(true, false);
            return;
        }
        int pawnId = response1.getData().get("pawn_id").getAsInt();
        SorryAPIResponse response2 = SorryAPI.updatePawnPosition(pawnId, 1, 2);
        assertEquals(true, response2.isSuccess());
    }

    @Test
    void createPawn() {
        GameModel game = new GameModel();
        game.createNewGame("[TEST] Created from SorryAPITest.createPawn()");
        PlayerModel player = new PlayerModel("[TEST] Created from SorryAPITest.createPawn()", game, Pawn.pawnColors.Blue);
        SorryAPIResponse response = SorryAPI.createPawn(player.getGamePlayerId(), 10, 12);
        assertEquals(true, response.isSuccess());
    }

    @Test
    void createComputerPlayer() {
        GameModel game = new GameModel();
        game.createNewGame("[TEST] Created from SorryAPITest.createComputerPlayer()");
        SorryAPIResponse response = SorryAPI.createComputerPlayer(
                game,
                PlayerModel.ComputerAbility.Naive,
                PlayerModel.ComputerFeels.Mean);
        assertEquals(true, response.isSuccess());
    }

    @Test
    void createHumanPlayer() {
        GameModel game = new GameModel();
        game.createNewGame("[TEST] Created from SorryAPITest.createHumanPlayer()");
        SorryAPIResponse response = SorryAPI.createHumanPlayer("[TEST] Created from SorryAPITest.createHumanPlayer()", game, Pawn.pawnColors.Red);
        assertEquals(true, response.isSuccess());
    }

    @Test
    void saveGame() {
        GameModel game = new GameModel();
        game.createNewGame("[TEST] Created from SorryAPITest.saveGame()");
        SorryAPIResponse response = SorryAPI.saveGame(game);
        assertEquals(true, response.isSuccess());
    }

    @Test
    void createGame() {
        SorryAPIResponse response = SorryAPI.createGame("[TEST] Created from SorryAPITest.createGame()");
        assertEquals(true, response.isSuccess());
    }

    @Test
    void getAllGames() {
        SorryAPIResponse response = SorryAPI.getAllGames();
        assertEquals(true, response.isSuccess());
        assertEquals("Successfully fetched games.", response.getMessage());
        assertNotEquals("{}", response.getData().toString());
    }

    @Test
    void verifyConnection() {
        SorryAPIResponse response = SorryAPI.verifyConnection();
        assertEquals(true, response.isSuccess());
        assertEquals("Successfully connected to endpoint.", response.getMessage());
        assertEquals("{}", response.getData().toString());
    }

}
